Running 1 test case...
[1;35;49m
0%   10   20   30   40   50   60   70   80   90   100%
|----|----|----|----|----|----|----|----|----|----|
[0;39;49m[1;34;49mEntering test module "test-multileg"
[0;39;49m[1;34;49mtests/onixs.biz/src/multileg-perf.cc:247: Entering test case "test_Perf_Encode_NewOrderMultileg"
[0;39;49m[1;36;49mParam<1000ul, raw::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void raw::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, raw::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 899.000000 ns
min: 49.000000 ns
mean: 57.793103 ns
variance : 15.828162 ns
max: 64.000000 ns
err: 0.132763 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<1000ul, ronixs::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void ronixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, ronixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 899.000000 ns
min: 42.000000 ns
mean: 43.143493 ns
variance : 0.587864 ns
max: 44.000000 ns
err: 0.025586 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<1000ul, onixs::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void onixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, onixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 899.000000 ns
min: 124.000000 ns
mean: 126.626251 ns
variance : 1.689011 ns
max: 130.000000 ns
err: 0.043369 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<1000ul, onixs::NewOrderMultileg, (smunix::buffer::AlignType)1, &(void onixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)1>&>(bool&, onixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)1>&&&))>
--------------------------------------------------------------------------------
count: 899.000000 ns
min: 118.000000 ns
mean: 121.127920 ns
variance : 1.281746 ns
max: 124.000000 ns
err: 0.037780 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<100000ul, raw::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void raw::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, raw::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 89999.000000 ns
min: 40.000000 ns
mean: 48.778231 ns
variance : 54.917885 ns
max: 63.000000 ns
err: 0.024702 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<100000ul, ronixs::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void ronixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, ronixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 89999.000000 ns
min: 35.000000 ns
mean: 39.361104 ns
variance : 0.612334 ns
max: 40.000000 ns
err: 0.002608 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<100000ul, onixs::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void onixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, onixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 89999.000000 ns
min: 111.000000 ns
mean: 116.906610 ns
variance : 2.345027 ns
max: 119.000000 ns
err: 0.005105 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<100000ul, onixs::NewOrderMultileg, (smunix::buffer::AlignType)1, &(void onixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)1>&>(bool&, onixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)1>&&&))>
--------------------------------------------------------------------------------
count: 89999.000000 ns
min: 108.000000 ns
mean: 115.550673 ns
variance : 14.080164 ns
max: 125.000000 ns
err: 0.012508 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<199000ul, raw::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void raw::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, raw::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 179099.000000 ns
min: 35.000000 ns
mean: 40.145696 ns
variance : 2.158349 ns
max: 44.000000 ns
err: 0.003471 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<199000ul, ronixs::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void ronixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, ronixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 179099.000000 ns
min: 35.000000 ns
mean: 41.141447 ns
variance : 18.436707 ns
max: 56.000000 ns
err: 0.010146 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<199000ul, onixs::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void onixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, onixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 179099.000000 ns
min: 110.000000 ns
mean: 118.128845 ns
variance : 10.080641 ns
max: 127.000000 ns
err: 0.007502 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<199000ul, onixs::NewOrderMultileg, (smunix::buffer::AlignType)1, &(void onixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)1>&>(bool&, onixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)1>&&&))>
--------------------------------------------------------------------------------
count: 179099.000000 ns
min: 106.000000 ns
mean: 110.333804 ns
variance : 3.223055 ns
max: 114.000000 ns
err: 0.004242 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<298000ul, raw::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void raw::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, raw::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 268199.000000 ns
min: 34.000000 ns
mean: 40.675017 ns
variance : 3.455108 ns
max: 44.000000 ns
err: 0.003589 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<298000ul, ronixs::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void ronixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, ronixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 268199.000000 ns
min: 34.000000 ns
mean: 39.364371 ns
variance : 0.871555 ns
max: 40.000000 ns
err: 0.001803 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<298000ul, onixs::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void onixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, onixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 268199.000000 ns
min: 111.000000 ns
mean: 117.445300 ns
variance : 18.593035 ns
max: 128.000000 ns
err: 0.008326 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<298000ul, onixs::NewOrderMultileg, (smunix::buffer::AlignType)1, &(void onixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)1>&>(bool&, onixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)1>&&&))>
--------------------------------------------------------------------------------
count: 268199.000000 ns
min: 108.000000 ns
mean: 111.655946 ns
variance : 2.645832 ns
max: 114.000000 ns
err: 0.003141 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<397000ul, raw::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void raw::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, raw::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 357299.000000 ns
min: 34.000000 ns
mean: 39.344168 ns
variance : 0.889883 ns
max: 40.000000 ns
err: 0.001578 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<397000ul, ronixs::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void ronixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, ronixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 357299.000000 ns
min: 34.000000 ns
mean: 39.354930 ns
variance : 0.875024 ns
max: 40.000000 ns
err: 0.001565 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<397000ul, onixs::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void onixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, onixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 357299.000000 ns
min: 109.000000 ns
mean: 119.033806 ns
variance : 8.599410 ns
max: 125.000000 ns
err: 0.004906 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<397000ul, onixs::NewOrderMultileg, (smunix::buffer::AlignType)1, &(void onixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)1>&>(bool&, onixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)1>&&&))>
--------------------------------------------------------------------------------
count: 357299.000000 ns
min: 106.000000 ns
mean: 114.368518 ns
variance : 7.792252 ns
max: 121.000000 ns
err: 0.004670 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<496000ul, raw::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void raw::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, raw::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 446399.000000 ns
min: 35.000000 ns
mean: 39.352362 ns
variance : 0.622357 ns
max: 40.000000 ns
err: 0.001181 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<496000ul, ronixs::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void ronixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, ronixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 446399.000000 ns
min: 34.000000 ns
mean: 40.442611 ns
variance : 2.966161 ns
max: 44.000000 ns
err: 0.002578 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<496000ul, onixs::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void onixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, onixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 446399.000000 ns
min: 107.000000 ns
mean: 118.811070 ns
variance : 2.567636 ns
max: 120.000000 ns
err: 0.002398 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<496000ul, onixs::NewOrderMultileg, (smunix::buffer::AlignType)1, &(void onixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)1>&>(bool&, onixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)1>&&&))>
--------------------------------------------------------------------------------
count: 446399.000000 ns
min: 104.000000 ns
mean: 113.986725 ns
variance : 10.239099 ns
max: 119.000000 ns
err: 0.004789 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<595000ul, raw::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void raw::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, raw::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 535499.000000 ns
min: 31.000000 ns
mean: 40.424653 ns
variance : 0.247837 ns
max: 41.000000 ns
err: 0.000680 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<595000ul, ronixs::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void ronixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, ronixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 535499.000000 ns
min: 31.000000 ns
mean: 40.432921 ns
variance : 0.247936 ns
max: 41.000000 ns
err: 0.000680 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<595000ul, onixs::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void onixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, onixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 535499.000000 ns
min: 109.000000 ns
mean: 119.956752 ns
variance : 4.326020 ns
max: 124.000000 ns
err: 0.002842 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<595000ul, onixs::NewOrderMultileg, (smunix::buffer::AlignType)1, &(void onixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)1>&>(bool&, onixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)1>&&&))>
--------------------------------------------------------------------------------
count: 535499.000000 ns
min: 105.000000 ns
mean: 114.323808 ns
variance : 10.116850 ns
max: 118.000000 ns
err: 0.004347 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<694000ul, raw::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void raw::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, raw::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 624599.000000 ns
min: 31.000000 ns
mean: 40.431747 ns
variance : 0.248912 ns
max: 41.000000 ns
err: 0.000631 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<694000ul, ronixs::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void ronixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, ronixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 624599.000000 ns
min: 31.000000 ns
mean: 40.434434 ns
variance : 0.247776 ns
max: 41.000000 ns
err: 0.000630 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<694000ul, onixs::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void onixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, onixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 624599.000000 ns
min: 110.000000 ns
mean: 121.967104 ns
variance : 0.869510 ns
max: 124.000000 ns
err: 0.001180 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<694000ul, onixs::NewOrderMultileg, (smunix::buffer::AlignType)1, &(void onixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)1>&>(bool&, onixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)1>&&&))>
--------------------------------------------------------------------------------
count: 624599.000000 ns
min: 105.000000 ns
mean: 114.444397 ns
variance : 8.995421 ns
max: 117.000000 ns
err: 0.003795 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<793000ul, raw::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void raw::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, raw::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 713699.000000 ns
min: 31.000000 ns
mean: 40.425461 ns
variance : 0.247543 ns
max: 41.000000 ns
err: 0.000589 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<793000ul, ronixs::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void ronixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, ronixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 713699.000000 ns
min: 31.000000 ns
mean: 40.445532 ns
variance : 0.249286 ns
max: 41.000000 ns
err: 0.000591 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<793000ul, onixs::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void onixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, onixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 713699.000000 ns
min: 111.000000 ns
mean: 116.312239 ns
variance : 3.274865 ns
max: 120.000000 ns
err: 0.002142 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<793000ul, onixs::NewOrderMultileg, (smunix::buffer::AlignType)1, &(void onixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)1>&>(bool&, onixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)1>&&&))>
--------------------------------------------------------------------------------
count: 713699.000000 ns
min: 104.000000 ns
mean: 113.879238 ns
variance : 11.179119 ns
max: 118.000000 ns
err: 0.003958 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<892000ul, raw::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void raw::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, raw::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 802799.000000 ns
min: 35.000000 ns
mean: 39.343300 ns
variance : 0.620862 ns
max: 40.000000 ns
err: 0.000879 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<892000ul, ronixs::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void ronixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, ronixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 802799.000000 ns
min: 34.000000 ns
mean: 39.351362 ns
variance : 0.814953 ns
max: 40.000000 ns
err: 0.001008 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<892000ul, onixs::NewOrderMultileg, (smunix::buffer::AlignType)0, &(void onixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&>(bool&, onixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)0>&&&))>
--------------------------------------------------------------------------------
count: 802799.000000 ns
min: 110.000000 ns
mean: 121.263482 ns
variance : 154.842102 ns
max: 191.000000 ns
err: 0.013888 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;36;49mParam<892000ul, onixs::NewOrderMultileg, (smunix::buffer::AlignType)1, &(void onixs::Encode_NewOrderMultileg<smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)1>&>(bool&, onixs::NewOrderMultileg&, smunix::buffer::Message<unsigned char, (unsigned short)192, (smunix::buffer::AlignType)1>&&&))>
--------------------------------------------------------------------------------
count: 802799.000000 ns
min: 106.000000 ns
mean: 114.028242 ns
variance : 3.906194 ns
max: 117.000000 ns
err: 0.002206 ns
--------------------------------------------------------------------------------

[0;39;49m
[1;35;49m***************************************************
[0;39;49m[1;36;49mTest case test_Perf_Encode_NewOrderMultileg did not check any assertions[0;39;49m
[1;34;49mtests/onixs.biz/src/multileg-perf.cc:247: Leaving test case "test_Perf_Encode_NewOrderMultileg"; testing time: 1083662369us
[0;39;49m[1;35;49m[0;39;49m[1;34;49mLeaving test module "test-multileg"; testing time: 1083662504us
[0;39;49m
Test module "test-multileg" has passed with:
  1 test case out of 1 passed

  Test case "test_Perf_Encode_NewOrderMultileg" has passed

