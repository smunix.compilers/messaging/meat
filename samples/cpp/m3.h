
#pragma once

/*
File Generated from : samples/meat/m3.meat
Author : Providence Salumu <Providence.Salumu@smunix.com>
File Generated from : samples/meat/m3.meat
using "build/scratch/grill/grill samples/meat/m3.meat &> samples/meat/m3.h"
Copyright 2015 SMUNIX Inc.
*/

#include <type_traits>
#include <smunix/core-module.hh>

namespace m3 {
  namespace s = smunix;
  
  namespace mm1 {
    namespace s = smunix;
    
    namespace mmm1 {
      namespace s = smunix;
      enum class Fruits {
        Apple = 1,
        Avocado = 3,
      };
      enum class Animals {
        Gorilla = 1,
        Chimp = 1,
      };
      struct Simple1 {
        s::data<char> _f1;
        
        // all accessors - get
        char& f1() { return *_f1; }
        char const& f1() const { return *_f1; }
        
        // all fields
        template<class T> static void describe(T&& t) {
          Simple1 lSimple1;
          using Describe = s::fields::Describe<typename std::decay<T>::type>;
          Describe::template apply<char>("f1", s::p<uint8_t>(s::addr(lSimple1.f1())) - s::p<uint8_t>(s::addr(lSimple1)), std::forward<T>(t));
        }
      };
      struct Simple2 {
        s::data<char> _f1;
        s::data<uint8_t> _f2;
        
        // all accessors - get
        char& f1() { return *_f1; }
        char const& f1() const { return *_f1; }
        uint8_t& f2() { return *_f2; }
        uint8_t const& f2() const { return *_f2; }
        
        // all fields
        template<class T> static void describe(T&& t) {
          Simple2 lSimple2;
          using Describe = s::fields::Describe<typename std::decay<T>::type>;
          Describe::template apply<char>("f1", s::p<uint8_t>(s::addr(lSimple2.f1())) - s::p<uint8_t>(s::addr(lSimple2)), std::forward<T>(t));
          Describe::template apply<uint8_t>("f2", s::p<uint8_t>(s::addr(lSimple2.f2())) - s::p<uint8_t>(s::addr(lSimple2)), std::forward<T>(t));
        }
      };
    } // namespace mmm1
    
    namespace mmm2 {
      namespace s = smunix;
      enum class Fruits {
        Apple = 1,
      };
      enum class Animals {
        Human = 1,
        Chimp = 1,
      };
      struct Simple1 {
        s::data<char> _f1;
        
        // all accessors - get
        char& f1() { return *_f1; }
        char const& f1() const { return *_f1; }
        
        // all fields
        template<class T> static void describe(T&& t) {
          Simple1 lSimple1;
          using Describe = s::fields::Describe<typename std::decay<T>::type>;
          Describe::template apply<char>("f1", s::p<uint8_t>(s::addr(lSimple1.f1())) - s::p<uint8_t>(s::addr(lSimple1)), std::forward<T>(t));
        }
      };
      struct Simple2 {
        s::data<char> _f1;
        s::data<uint8_t> _f2;
        
        // all accessors - get
        char& f1() { return *_f1; }
        char const& f1() const { return *_f1; }
        uint8_t& f2() { return *_f2; }
        uint8_t const& f2() const { return *_f2; }
        
        // all fields
        template<class T> static void describe(T&& t) {
          Simple2 lSimple2;
          using Describe = s::fields::Describe<typename std::decay<T>::type>;
          Describe::template apply<char>("f1", s::p<uint8_t>(s::addr(lSimple2.f1())) - s::p<uint8_t>(s::addr(lSimple2)), std::forward<T>(t));
          Describe::template apply<uint8_t>("f2", s::p<uint8_t>(s::addr(lSimple2.f2())) - s::p<uint8_t>(s::addr(lSimple2)), std::forward<T>(t));
        }
      };
      struct Extend1 {
        s::data<mmm1::Simple1> _l3s1;
        s::data<mm1::mmm1::Simple1> _l2l3s1;
        s::data<m3::mm1::mmm1::Simple1> _l1l2l3s1;
        s::data<char> _f1;
        
        // all accessors - get
        mmm1::Simple1& l3s1() { return *_l3s1; }
        mmm1::Simple1 const& l3s1() const { return *_l3s1; }
        mm1::mmm1::Simple1& l2l3s1() { return *_l2l3s1; }
        mm1::mmm1::Simple1 const& l2l3s1() const { return *_l2l3s1; }
        m3::mm1::mmm1::Simple1& l1l2l3s1() { return *_l1l2l3s1; }
        m3::mm1::mmm1::Simple1 const& l1l2l3s1() const { return *_l1l2l3s1; }
        char& f1() { return *_f1; }
        char const& f1() const { return *_f1; }
        
        // all fields
        template<class T> static void describe(T&& t) {
          Extend1 lExtend1;
          using Describe = s::fields::Describe<typename std::decay<T>::type>;
          Describe::template apply<mmm1::Simple1>("l3s1", s::p<uint8_t>(s::addr(lExtend1.l3s1())) - s::p<uint8_t>(s::addr(lExtend1)), std::forward<T>(t));
          Describe::template apply<mm1::mmm1::Simple1>("l2l3s1", s::p<uint8_t>(s::addr(lExtend1.l2l3s1())) - s::p<uint8_t>(s::addr(lExtend1)), std::forward<T>(t));
          Describe::template apply<m3::mm1::mmm1::Simple1>("l1l2l3s1", s::p<uint8_t>(s::addr(lExtend1.l1l2l3s1())) - s::p<uint8_t>(s::addr(lExtend1)), std::forward<T>(t));
          Describe::template apply<char>("f1", s::p<uint8_t>(s::addr(lExtend1.f1())) - s::p<uint8_t>(s::addr(lExtend1)), std::forward<T>(t));
        }
      };
      struct Extend2 {
        s::data<Simple1> _p2s1;
        s::data<mm1::mmm2::Simple2> _l2l32s2;
        s::data<m3::mm1::mmm1::Simple1> _l1l2l3s1;
        s::data<char> _f1;
        s::data<uint8_t> _f2;
        
        // all accessors - get
        Simple1& p2s1() { return *_p2s1; }
        Simple1 const& p2s1() const { return *_p2s1; }
        mm1::mmm2::Simple2& l2l32s2() { return *_l2l32s2; }
        mm1::mmm2::Simple2 const& l2l32s2() const { return *_l2l32s2; }
        m3::mm1::mmm1::Simple1& l1l2l3s1() { return *_l1l2l3s1; }
        m3::mm1::mmm1::Simple1 const& l1l2l3s1() const { return *_l1l2l3s1; }
        char& f1() { return *_f1; }
        char const& f1() const { return *_f1; }
        uint8_t& f2() { return *_f2; }
        uint8_t const& f2() const { return *_f2; }
        
        // all fields
        template<class T> static void describe(T&& t) {
          Extend2 lExtend2;
          using Describe = s::fields::Describe<typename std::decay<T>::type>;
          Describe::template apply<Simple1>("p2s1", s::p<uint8_t>(s::addr(lExtend2.p2s1())) - s::p<uint8_t>(s::addr(lExtend2)), std::forward<T>(t));
          Describe::template apply<mm1::mmm2::Simple2>("l2l32s2", s::p<uint8_t>(s::addr(lExtend2.l2l32s2())) - s::p<uint8_t>(s::addr(lExtend2)), std::forward<T>(t));
          Describe::template apply<m3::mm1::mmm1::Simple1>("l1l2l3s1", s::p<uint8_t>(s::addr(lExtend2.l1l2l3s1())) - s::p<uint8_t>(s::addr(lExtend2)), std::forward<T>(t));
          Describe::template apply<char>("f1", s::p<uint8_t>(s::addr(lExtend2.f1())) - s::p<uint8_t>(s::addr(lExtend2)), std::forward<T>(t));
          Describe::template apply<uint8_t>("f2", s::p<uint8_t>(s::addr(lExtend2.f2())) - s::p<uint8_t>(s::addr(lExtend2)), std::forward<T>(t));
        }
      };
    } // namespace mmm2
  } // namespace mm1
} // namespace m3

namespace m4 {
  namespace s = smunix;
  enum class Fruits {
    Apple = 1,
    Papaya = 1,
  };
  
  namespace mm1 {
    namespace s = smunix;
    enum class InternalFruits {
      Orange = 2,
      Mango = 3,
    };
    
    namespace mmm1 {
      namespace s = smunix;
      struct RecordL2 {
        s::data<char> _f0;
        
        // all accessors - get
        char& f0() { return *_f0; }
        char const& f0() const { return *_f0; }
        
        // all fields
        template<class T> static void describe(T&& t) {
          RecordL2 lRecordL2;
          using Describe = s::fields::Describe<typename std::decay<T>::type>;
          Describe::template apply<char>("f0", s::p<uint8_t>(s::addr(lRecordL2.f0())) - s::p<uint8_t>(s::addr(lRecordL2)), std::forward<T>(t));
        }
      };
    } // namespace mmm1
  } // namespace mm1
} // namespace m4
