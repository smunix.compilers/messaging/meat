#include <boost/test/unit_test.hpp>
#include <smunix/core/memory.hh>

BOOST_AUTO_TEST_CASE(test_dummy)
{
  BOOST_CHECK_MESSAGE(1==1, "1 EQ 1");
}

struct A {
  uint32_t v = 0xbadbeef;
  smunix::p<bool> del = nullptr;
  ~A() { *del = true; }
};

BOOST_AUTO_TEST_CASE(test_pointer_p)
{
  using namespace smunix;
  bool b = false;
  p<A> p = new A;
  p->del = &b;
  BOOST_CHECK_MESSAGE(p->v == 0xbadbeef, "access member var through pointer");
  delete p;
  BOOST_CHECK_MESSAGE(b, "pointer is deleted");
}

BOOST_AUTO_TEST_CASE(test_pointer_sp)
{
  using namespace smunix;
  bool b = false;
  {
    sp<A> p = std::make_shared<A>();
    p->del = &b;
    BOOST_CHECK_MESSAGE(p->v == 0xbadbeef, "access member var through pointer");
  }
  BOOST_CHECK_MESSAGE(b, "pointer is deleted");
}
