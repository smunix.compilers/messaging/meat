#pragma once
#include <iostream>
#include <sstream>
#include <iomanip>
#include <ctime>
#include <chrono>

#include <boost/algorithm/string/replace.hpp>
#include <boost/lexical_cast.hpp>

#include <smunix/core/memory.hh>
#include <smunix/core/array.hh>
#include <smunix/core/ref.hh>
#include <smunix/core/buffer.hh>
#include <smunix/core/cache.hh>
#include <smunix/core/utils.hh>
