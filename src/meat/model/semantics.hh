#pragma once
#include <boost/preprocessor.hpp>
#include <smunix/core/memory.hh>
#include <smunix/meat/alloc.hh>
#include <string>
#include <map>
#include <set>
#include <unordered_set>
#include <algorithm>

namespace meat {
  namespace s = smunix;

  template<class T, class U, U T::*M> struct hash {
    using argument_type = s::sp<T>;
    using result_type = size_t;
    result_type operator()(argument_type a) {
      return std::hash<U>()(*a.*M);
    }
    result_type operator()(argument_type a) const {
      return std::hash<U>()(*a.*M);
    }
  };
  template<class T> struct clock_comp {
    using argument_type = s::sp<T>;
    using result_type = bool;
    using first_argument_type = argument_type;
    using second_argument_type = argument_type;
    result_type operator()(s::r<argument_type const> a, s::r<argument_type const> b) const {
      return a->clock() < b->clock();
    }
  };
  template<class T> using ClockSet = std::set<s::sp<T>, clock_comp<T>>;
  template<class K, class V> using Map = std::map<K,V>;
  template<class K, class T> ClockSet<T> clocks(s::r<Map<K, s::sp<T>> const> aMap) {
    ClockSet<T> cs;
    for (auto& kv : aMap)
      cs.insert(kv.second);
    return cs;
  }
  struct Semantic {
    using clock_t = size_t;
    enum class Type {
      File,
	Import,
	Definition,
	EnumEntry,
	Field,	
	TypeDefinition,
	TypePrefix,
	TypeSfxQual,
	ScalarType,
	};
    using sp = s::sp<Semantic>;
    using Map = std::map<std::string, sp>;
    Semantic(Type tp) : semanticType(tp) { clock(mClock); }
    Type type() const { return semanticType; }
    virtual s::r<std::string const> id() { static std::string str = "INVALID_ID_STR"; return str; }
    virtual clock_t clock() const {
      return mClock;
    }
  private:
    static void clock(s::r<clock_t> ac) {
      static clock_t wall = 0;
      ac = (++wall);
    }
    Type semanticType;
    clock_t mClock;
  };

  template<Semantic::Type T>
  struct Cast : public Semantic {
    Cast() : Semantic(T) {}
    static constexpr Semantic::Type ctype() { return T; }
  };

  template<class T, class U> s::sp<T> cast(s::sp<U> u) {
    if (u->type() == T::ctype()) {
      return s::asp<T>(u);
    }
    return s::sp<T>(nullptr);
  }

  struct ScalarType : Cast<Semantic::Type::ScalarType> {
    using sp = s::sp<ScalarType>;
    ScalarType(s::r<std::string const> name) : name(name) {}
    s::r<std::string const> id() override {
      return name;
    }
    
    std::string name;
  };
  
  struct TypeSfxQual : Cast<Semantic::Type::TypeSfxQual> {
    using sp = s::sp<TypeSfxQual>;
    enum class Selector { TypeSfxQual, TypeSfxQualTag, TypeSfxQualGrp, };
    TypeSfxQual(Selector slr = Selector::TypeSfxQual) : suffixType(slr) {}
    Selector type() const { return suffixType; }
  private:
    Selector suffixType;
  };

  template<TypeSfxQual::Selector S> struct SfxCast : TypeSfxQual {
    static constexpr TypeSfxQual::Selector ctype() { return S; }
    SfxCast() : TypeSfxQual(ctype()) {}
  };
  
  struct TypeSfxQualGrp : SfxCast<TypeSfxQual::Selector::TypeSfxQualGrp> {
    using sp = s::sp<TypeSfxQualGrp>;
    TypeSfxQualGrp(uint16_t tagnb) : tag(tagnb) {}
    
    uint16_t tag;
  };

  struct TypeSfxQualTag : SfxCast<TypeSfxQual::Selector::TypeSfxQualTag> {
    using sp = s::sp<TypeSfxQualTag>;
    TypeSfxQualTag(uint16_t tagnb, ScalarType::sp stp) : tag(tagnb), type(stp) {}
    
    uint16_t		tag;
    ScalarType::sp	type;
  };

  struct TypePrefix : Cast<Semantic::Type::TypePrefix> {
    using sp = s::sp<TypePrefix>;
    enum class Selector { ReferencedTypePrefix, OptionalTypePrefix, FixArrTypePrefix, DynArrTypePrefix, ScalarTypePrefix };
    TypePrefix(Selector slr) : prefixType(slr) {}
    Selector type() const { return prefixType; }
  private:
    Selector prefixType;
  };

  template<TypePrefix::Selector T> struct PrefixCast : TypePrefix {
    static constexpr TypePrefix::Selector ctype() { return T;}
    PrefixCast() : TypePrefix(ctype()) {}
  };
  
  struct ReferencedTypePrefix : PrefixCast<TypePrefix::Selector::ReferencedTypePrefix> {
    using sp = s::sp<ReferencedTypePrefix>;
    ReferencedTypePrefix(TypePrefix::sp type) : type(type) {}

    TypePrefix::sp type;
  };

  struct OptionalTypePrefix : PrefixCast<TypePrefix::Selector::OptionalTypePrefix> {
    using sp = s::sp<OptionalTypePrefix>;
    OptionalTypePrefix(TypePrefix::sp type) : type(type)  {}

    TypePrefix::sp type;
  };

  struct FixArrTypePrefix : PrefixCast<TypePrefix::Selector::FixArrTypePrefix> {
    using sp = s::sp<FixArrTypePrefix>;
    FixArrTypePrefix(TypePrefix::sp type, size_t len) : type(type), len(len)  {}

    TypePrefix::sp	type;
    size_t		len;
  };

  struct DynArrTypePrefix : PrefixCast<TypePrefix::Selector::DynArrTypePrefix> {
    using sp = s::sp<DynArrTypePrefix>;
    DynArrTypePrefix(TypePrefix::sp type) : type(type)  {}

    TypePrefix::sp type;
  };

  struct ScalarTypePrefix : PrefixCast<TypePrefix::Selector::ScalarTypePrefix> {
    using sp = s::sp<ScalarTypePrefix>;
    ScalarTypePrefix(ScalarType::sp type) : type(type) {}

    ScalarType::sp type;
  };

  struct TypeDefinition : Cast<Semantic::Type::TypeDefinition> {
    using sp = s::sp<TypeDefinition>;
    TypeDefinition(TypePrefix::sp prefix, TypeSfxQual::sp suffixQual) : prefix(prefix), suffixQual(suffixQual) {}
    TypeDefinition(TypePrefix::sp prefix) : prefix(prefix), suffixQual(nullptr) {}
    
    TypePrefix::sp	prefix;
    TypeSfxQual::sp	suffixQual;    
  };

  struct Field : Cast<Semantic::Type::Field> {
    using sp = s::sp<Field>;
    using Map = std::map<std::string, sp>;
    Field(s::r<std::string const> lbl, TypeDefinition::sp typeDef) : label(lbl), typeDef(typeDef) {}
    s::r<std::string const> id() override {
      return label;
    }
    
    std::string		label;
    TypeDefinition::sp	typeDef;
  };

  struct Definition : Cast<Semantic::Type::Definition> {
    using sp = s::sp<Definition>;
    using Map = std::map<std::string, sp>;
    enum class Selector { Module, Record, Enum};
    Definition(Selector slr) : definitionType(slr) {}
    Selector type() const { return definitionType; }
    
    Selector definitionType;
  };

  template<Definition::Selector S> struct DefinitionCast : Definition {
    DefinitionCast() : Definition(S) {}
    static constexpr Definition::Selector ctype() { return S; }
  };

  struct Enum : DefinitionCast<Definition::Selector::Enum> {
    using sp = s::sp<Enum>;

    struct Entry : Cast<Semantic::Type::EnumEntry> {
      using sp = s::sp<Entry>;
      using Map = std::map<std::string, sp>;
      Entry(s::r<std::string const> lbl, int64_t value) : label(lbl), value(value) {}
      s::r<std::string const> id() override {
	return label;
      }
    
      std::string	label;
      int64_t		value;
    };
    
    Enum(s::r<std::string const> name, s::sp<Entry::Map> entries) : name(name), entries(entries) {}
    s::r<std::string const> id() override {
      return name;
    }

    std::string		name;
    s::sp<Entry::Map>	entries;
  };

  struct Record : DefinitionCast<Definition::Selector::Record> {
    using sp = s::sp<Record>;
    using Map = std::map<std::string, sp>;
    Record(s::r<std::string const> name, s::sp<Field::Map> fieldxs, s::sp<Field::Map> extFieldxs) : name(name), fieldxs(fieldxs), extFieldxs(extFieldxs) {}
    Record(s::r<std::string const> name, s::sp<Field::Map> fieldxs) : name(name), fieldxs(fieldxs), extFieldxs(nullptr) {}
    s::r<std::string const> id() override {
      return name;
    }

    std::string		name;
    s::sp<Field::Map>	fieldxs;
    s::sp<Field::Map>	extFieldxs;
  };

  struct Module : DefinitionCast<Definition::Selector::Module> {
    using sp = s::sp<Module>;
    using Map = std::map<std::string, sp>;
    Module(s::r<std::string const> name, s::sp<Definition::Map> definitionxs) : name(name), definitions(definitionxs) {}
    s::r<std::string const> id() override {
      return name;
    }

    std::string		name;
    s::sp<Definition::Map> definitions;
  };

  struct Import : Cast<Semantic::Type::Import> {
    using sp = s::sp<Import>;
    Import(s::r<std::string const> filename) : filename(filename) {}
    s::r<std::string const> id() override {
      return filename;
    }
    
    friend bool operator<(s::r<Import const> lhs, s::r<Import const> rhs) {
      return lhs.filename < rhs.filename;
    }
    friend bool operator==(s::r<Import const> lhs, s::r<Import const> rhs) {
      return lhs.filename == rhs.filename;
    }
    friend bool operator!=(s::r<Import const> lhs, s::r<Import const> rhs) {
      return lhs.filename != rhs.filename;
    }
    friend bool operator>(s::r<Import const> lhs, s::r<Import const> rhs) {
      return (lhs != rhs) and (lhs < rhs);
    }
    
    std::string filename;
    using Set = std::unordered_set<sp, hash<Import, std::string, &Import::filename>>;
  };

  struct File : Cast<Semantic::Type::File> {
    using sp = s::sp<File>;
    File(s::r<std::string const> filename, s::sp<Import::Set> importxs, s::sp<Module::Map> modulexs) : filename(filename), imports(importxs), modules(modulexs) { }
    File(s::sp<Import::Set> importxs, s::sp<Module::Map> modulexs) : filename("INVALID_STR"), imports(importxs), modules(modulexs) { }
    s::r<std::string const> id() override {
      return filename;
    }
    
    std::string filename;
    s::sp<Import::Set> imports;
    s::sp<Module::Map> modules;
  };
} //namespace meat

namespace meat {
  namespace s = smunix;
  template<class T> using Const = T const;
  template<class T> using Id = T;
  
  template<class R, template<class> class A = s::sp, template<class> class Mod = Id> struct Switch {
    virtual ~Switch() {}
    
    virtual R on(A<Mod<File>>)			= 0;
    virtual R on(A<Mod<Import>>)		= 0;
    virtual R on(A<Mod<Definition>>)            = 0;
    virtual R on(A<Mod<Module>>)		= 0;
    virtual R on(A<Mod<Record>>)		= 0;
    virtual R on(A<Mod<Enum>>)			= 0;
    virtual R on(A<Mod<Enum::Entry>>)		= 0;
    virtual R on(A<Mod<Field>>)			= 0;
    virtual R on(A<Mod<TypeDefinition>>)	= 0;
    virtual R on(A<Mod<ScalarTypePrefix>>)	= 0;
    virtual R on(A<Mod<DynArrTypePrefix>>)	= 0;
    virtual R on(A<Mod<FixArrTypePrefix>>)	= 0;
    virtual R on(A<Mod<OptionalTypePrefix>>)	= 0;
    virtual R on(A<Mod<ReferencedTypePrefix>>)	= 0;
    virtual R on(A<Mod<TypePrefix>>)		= 0;
    virtual R on(A<Mod<TypeSfxQualTag>>)	= 0;
    virtual R on(A<Mod<TypeSfxQualGrp>>)	= 0;
    virtual R on(A<Mod<TypeSfxQual>>)		= 0;
    virtual R on(A<Mod<ScalarType>>)		= 0;
    virtual R on(A<Mod<Semantic>>)		= 0;
  };

  template<class Vis, template<class> class A = s::sp, template<class> class Mod = Id> struct Visitor : Switch<typename Vis::Return, A, Mod> {
    using R = typename Vis::Return;
    Visitor(s::r<Vis> vis) : vis(vis) {}
    
    R on(A<Mod<File>> aVal) override			{ return vis.visit(s::asr(aVal)); }
    R on(A<Mod<Import>> aVal) override			{ return vis.visit(s::asr(aVal)); }
    R on(A<Mod<Definition>> aVal) override		{ return vis.visit(s::asr(aVal)); }
    R on(A<Mod<Module>> aVal) override			{ return vis.visit(s::asr(aVal)); }
    R on(A<Mod<Record>> aVal) override			{ return vis.visit(s::asr(aVal)); }
    R on(A<Mod<Enum>> aVal) override			{ return vis.visit(s::asr(aVal)); }
    R on(A<Mod<Enum::Entry>> aVal) override		{ return vis.visit(s::asr(aVal)); }
    R on(A<Mod<Field>> aVal) override			{ return vis.visit(s::asr(aVal)); }
    R on(A<Mod<TypeDefinition>> aVal) override		{ return vis.visit(s::asr(aVal)); }
    R on(A<Mod<ScalarTypePrefix>> aVal) override	{ return vis.visit(s::asr(aVal)); }
    R on(A<Mod<DynArrTypePrefix>> aVal) override	{ return vis.visit(s::asr(aVal)); }
    R on(A<Mod<FixArrTypePrefix>> aVal) override	{ return vis.visit(s::asr(aVal)); }
    R on(A<Mod<OptionalTypePrefix>> aVal) override	{ return vis.visit(s::asr(aVal)); }
    R on(A<Mod<ReferencedTypePrefix>> aVal) override	{ return vis.visit(s::asr(aVal)); }
    R on(A<Mod<TypePrefix>> aVal) override		{ return vis.visit(s::asr(aVal)); }
    R on(A<Mod<TypeSfxQualTag>> aVal) override		{ return vis.visit(s::asr(aVal)); }
    R on(A<Mod<TypeSfxQualGrp>> aVal) override		{ return vis.visit(s::asr(aVal)); }
    R on(A<Mod<TypeSfxQual>> aVal) override		{ return vis.visit(s::asr(aVal)); }
    R on(A<Mod<ScalarType>> aVal) override		{ return vis.visit(s::asr(aVal)); }
    R on(A<Mod<Semantic>> aVal) override		{ return vis.visit(s::asr(aVal)); }

    s::r<Vis> vis;
  };
#define SWITCH(n) BOOST_PP_CAT(n, _SEQ)(BOOST_PP_CAT(n, _MAC))

  template<class Vis, template<class> class A = s::sp, template<class> class Mod = Id> struct ForEach : Visitor<Vis, A, Mod> {
    using base = Visitor<Vis, A, Mod>;
    ForEach(s::r<Vis> vis) : base(vis) {}
    
    void on(A<Mod<File>> aVal) override {
      this->vis.begin(s::asr(aVal));
      this->vis.visit(s::asr(aVal));
      if (s::asr(aVal).imports)
	for(auto& i : s::asr(s::asr(aVal).imports))
	  this->on(i);
      if(s::asr(aVal).modules)
	for (auto &v : clocks(s::asr(s::asr(aVal).modules)))
	  this->on(v);
      this->vis.end(s::asr(aVal));
    }
    void on(A<Mod<Import>> aVal) override			{
      this->vis.begin(s::asr(aVal));
      this->vis.visit(s::asr(aVal));
      this->vis.end(s::asr(aVal));
    }
    void on(A<Mod<Module>> aVal) override			{
      this->vis.begin(s::asr(aVal));
      this->vis.visit(s::asr(aVal));
      if (s::asr(aVal).definitions)
	for(auto& v: clocks(s::asr(s::asr(aVal).definitions)))
	  this->on(v);
      this->vis.end(s::asr(aVal));
    }
    void on(A<Mod<Definition>> aVal) override                 {
      this->vis.begin(s::asr(aVal));
      switch(s::asr(aVal).Definition::type()) {
#define Definition_SEQ(z)			\
	z(Record);				\
	z(Enum);				\
	z(Module);				\
	/**/
#define Definition_MAC(x) case Definition::Selector::x : this->on(cast<x>(aVal)); break;
	SWITCH(Definition);
      }
      this->vis.end(s::asr(aVal));
    }
    void on(A<Mod<Record>> aVal) override			{
      this->vis.begin(s::asr(aVal));
      this->vis.visit(s::asr(aVal));
      if (s::asr(aVal).extFieldxs)
	for(auto& v: clocks(s::asr(s::asr(aVal).extFieldxs)))
	  this->on(v);
      if (s::asr(aVal).fieldxs)
	for(auto& v: clocks(s::asr(s::asr(aVal).fieldxs)))
	  this->on(v);
      this->vis.end(s::asr(aVal));
    }
    void on(A<Mod<Enum>> aVal) override			{
      this->vis.begin(s::asr(aVal));
      this->vis.visit(s::asr(aVal));
      if (s::asr(aVal).entries)
	for(auto& v: clocks(s::asr(s::asr(aVal).entries)))
	  this->on(v);
      this->vis.end(s::asr(aVal));
    }
    void on(A<Mod<Enum::Entry>> aVal) override		{
      this->vis.begin(s::asr(aVal));
      this->vis.visit(s::asr(aVal));
      this->vis.end(s::asr(aVal));
    }
    void on(A<Mod<Field>> aVal) override			{
      this->vis.begin(s::asr(aVal));
      this->vis.visit(s::asr(aVal));
      if (s::asr(aVal).typeDef)	this->on(s::asr(aVal).typeDef);
      this->vis.end(s::asr(aVal));
    }
    void on(A<Mod<TypeDefinition>> aVal) override		{
      this->vis.begin(s::asr(aVal));
      this->vis.visit(s::asr(aVal));
      if (s::asr(aVal).prefix) this->on(s::asr(aVal).prefix);
      if (s::asr(aVal).suffixQual) this->on(s::asr(aVal).suffixQual);
      this->vis.end(s::asr(aVal));
    }
    void on(A<Mod<ScalarTypePrefix>> aVal) override		{
      this->vis.begin(s::asr(aVal));
      this->vis.visit(s::asr(aVal));
      if (s::asr(aVal).type) this->on(s::asr(aVal).type);
      this->vis.end(s::asr(aVal));
    }
    void on(A<Mod<DynArrTypePrefix>> aVal) override		{
      this->vis.begin(s::asr(aVal));
      this->vis.visit(s::asr(aVal));
      if (s::asr(aVal).type) this->on(s::asr(aVal).type);
      this->vis.end(s::asr(aVal));
    }
    void on(A<Mod<FixArrTypePrefix>> aVal) override		{
      this->vis.begin(s::asr(aVal));
      this->vis.visit(s::asr(aVal));
      if (s::asr(aVal).type) this->on(s::asr(aVal).type);
      this->vis.end(s::asr(aVal));
    }
    void on(A<Mod<OptionalTypePrefix>> aVal) override		{
      this->vis.begin(s::asr(aVal));
      this->vis.visit(s::asr(aVal));
      if (s::asr(aVal).type) this->on(s::asr(aVal).type);
      this->vis.end(s::asr(aVal));
    }
    void on(A<Mod<ReferencedTypePrefix>> aVal) override	{
      this->vis.begin(s::asr(aVal));
      this->vis.visit(s::asr(aVal));
      if (s::asr(aVal).type) this->on(s::asr(aVal).type);
      this->vis.end(s::asr(aVal));
    }
    void on(A<Mod<TypePrefix>> aVal) override			{
      this->vis.begin(s::asr(aVal));
      switch(s::asr(aVal).TypePrefix::type()) {
#define TypePrefix_SEQ(z)			\
	z(ScalarTypePrefix);			\
	z(ReferencedTypePrefix);		\
	z(OptionalTypePrefix);			\
	z(DynArrTypePrefix);			\
	z(FixArrTypePrefix);			\
	/**/
#define TypePrefix_MAC(x) case TypePrefix::Selector::x : this->on(cast<x>(aVal)); break;
	SWITCH(TypePrefix); 
      }
      this->vis.end(s::asr(aVal));
    }
    void on(A<Mod<TypeSfxQualTag>> aVal) override		{
      this->vis.begin(s::asr(aVal));
      this->vis.visit(s::asr(aVal));
      this->vis.visit(s::asr(s::asr(aVal).type));      
      this->vis.end(s::asr(aVal));
    }
    void on(A<Mod<TypeSfxQualGrp>> aVal) override		{
      this->vis.begin(s::asr(aVal));
      this->vis.visit(s::asr(aVal));
      this->vis.end(s::asr(aVal));
    }
    void on(A<Mod<TypeSfxQual>> aVal) override		{
      this->vis.begin(s::asr(aVal));
      switch(s::asr(aVal).TypeSfxQual::type()) {
#define TypeSfxQual_SEQ(z)			\
	z(TypeSfxQualTag);			\
	z(TypeSfxQualGrp);			\
	/**/
#define TypeSfxQual_MAC(x) case TypeSfxQual::Selector::x : this->on(cast<x>(aVal)); break;
	SWITCH(TypeSfxQual);
      default: break;
      }
      this->vis.end(s::asr(aVal));
    }
    void on(A<Mod<ScalarType>> aVal) override			{
      this->vis.begin(s::asr(aVal));
      this->vis.visit(s::asr(aVal));
      this->vis.end(s::asr(aVal));
    }
    void on(A<Mod<Semantic>> aVal) override			{
      this->vis.begin(s::asr(aVal));
      this->vis.visit(s::asr(aVal));
      this->vis.end(s::asr(aVal));
    }
    
  };

} // namespace meat

