#pragma once

#include <string>
#include <map>
#include <unordered_set>
#include <vector>
#include <smunix/meat/parser.hh>
#include <smunix/meat/semantics.hh>

#undef YY_DECL
#define YY_DECL meat::yy::parser::symbol_type yylex(meat::driver& driver)
YY_DECL;

namespace meat {
  namespace s = smunix;
  
  class driver {
    template<class T> using Vec = std::vector<T>;
    template<class T, class H = std::hash<T>> using Set = std::unordered_set<T, H>;
    using filename = std::string;
  public:
    driver();
    virtual ~driver();
    // scan begin/end
    void begin();
    void end();
    // parse
    int parse(s::r<filename const> f);
    // error handling
    void error(yy::parser::location_type const& loc, std::string const& msg);
    void error(std::string const& msg);

    void trace_parse(bool v);
    void trace_scan(bool v);

    void set(File::sp afile) {
      afile->filename = filenames.back();
      files.emplace_back(afile);
    }

    template<class Vis> void visit(s::r<Vis> vis) {
      ForEach<Vis> fVis(vis);
      for(auto& file : files)
	fVis.on(file);
    }

    Set<filename> filters;
    Vec<filename> filenames;
    Vec<File::sp> files;
    bool trace_parsing;
  };
} // namespace cbbcc
