// A Bison parser, made by GNU Bison 3.0.4.

// Skeleton interface for Bison LALR(1) parsers in C++

// Copyright (C) 2002-2015 Free Software Foundation, Inc.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// As a special exception, you may create a larger work that contains
// part or all of the Bison parser skeleton and distribute that work
// under terms of your choice, so long as that work isn't itself a
// parser generator using the skeleton or a modified version thereof
// as a parser skeleton.  Alternatively, if you modify or redistribute
// the parser skeleton itself, you may (at your option) remove this
// special exception, which will cause the skeleton and the resulting
// Bison output files to be licensed under the GNU General Public
// License without this special exception.

// This special exception was added by the Free Software Foundation in
// version 2.2 of Bison.

/**
 ** \file src/meat/model/parser.hh
 ** Define the  meat::yy ::parser class.
 */

// C++ LALR(1) parser skeleton written by Akim Demaille.

#ifndef YY_YY_SRC_MEAT_MODEL_PARSER_HH_INCLUDED
# define YY_YY_SRC_MEAT_MODEL_PARSER_HH_INCLUDED
// //                    "%code requires" blocks.
#line 17 "pgen/parser.y" // lalr1.cc:392

#include <string>
namespace meat {
    class driver;
} // namespace meat
#include <smunix/meat/semantics.hh>

#line 52 "src/meat/model/parser.hh" // lalr1.cc:392

# include <cassert>
# include <cstdlib> // std::abort
# include <iostream>
# include <stdexcept>
# include <string>
# include <vector>
# include "stack.hh"
# include "location.hh"
#include <typeinfo>
#ifndef YYASSERT
# include <cassert>
# define YYASSERT assert
#endif


#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif

#line 13 "pgen/parser.y" // lalr1.cc:392
namespace  meat { namespace yy  {
#line 129 "src/meat/model/parser.hh" // lalr1.cc:392



  /// A char[S] buffer to store and retrieve objects.
  ///
  /// Sort of a variant, but does not keep track of the nature
  /// of the stored data, since that knowledge is available
  /// via the current state.
  template <size_t S>
  struct variant
  {
    /// Type of *this.
    typedef variant<S> self_type;

    /// Empty construction.
    variant ()
      : yytypeid_ (YY_NULLPTR)
    {}

    /// Construct and fill.
    template <typename T>
    variant (const T& t)
      : yytypeid_ (&typeid (T))
    {
      YYASSERT (sizeof (T) <= S);
      new (yyas_<T> ()) T (t);
    }

    /// Destruction, allowed only if empty.
    ~variant ()
    {
      YYASSERT (!yytypeid_);
    }

    /// Instantiate an empty \a T in here.
    template <typename T>
    T&
    build ()
    {
      YYASSERT (!yytypeid_);
      YYASSERT (sizeof (T) <= S);
      yytypeid_ = & typeid (T);
      return *new (yyas_<T> ()) T;
    }

    /// Instantiate a \a T in here from \a t.
    template <typename T>
    T&
    build (const T& t)
    {
      YYASSERT (!yytypeid_);
      YYASSERT (sizeof (T) <= S);
      yytypeid_ = & typeid (T);
      return *new (yyas_<T> ()) T (t);
    }

    /// Accessor to a built \a T.
    template <typename T>
    T&
    as ()
    {
      YYASSERT (*yytypeid_ == typeid (T));
      YYASSERT (sizeof (T) <= S);
      return *yyas_<T> ();
    }

    /// Const accessor to a built \a T (for %printer).
    template <typename T>
    const T&
    as () const
    {
      YYASSERT (*yytypeid_ == typeid (T));
      YYASSERT (sizeof (T) <= S);
      return *yyas_<T> ();
    }

    /// Swap the content with \a other, of same type.
    ///
    /// Both variants must be built beforehand, because swapping the actual
    /// data requires reading it (with as()), and this is not possible on
    /// unconstructed variants: it would require some dynamic testing, which
    /// should not be the variant's responsability.
    /// Swapping between built and (possibly) non-built is done with
    /// variant::move ().
    template <typename T>
    void
    swap (self_type& other)
    {
      YYASSERT (yytypeid_);
      YYASSERT (*yytypeid_ == *other.yytypeid_);
      std::swap (as<T> (), other.as<T> ());
    }

    /// Move the content of \a other to this.
    ///
    /// Destroys \a other.
    template <typename T>
    void
    move (self_type& other)
    {
      build<T> ();
      swap<T> (other);
      other.destroy<T> ();
    }

    /// Copy the content of \a other to this.
    template <typename T>
    void
    copy (const self_type& other)
    {
      build<T> (other.as<T> ());
    }

    /// Destroy the stored \a T.
    template <typename T>
    void
    destroy ()
    {
      as<T> ().~T ();
      yytypeid_ = YY_NULLPTR;
    }

  private:
    /// Prohibit blind copies.
    self_type& operator=(const self_type&);
    variant (const self_type&);

    /// Accessor to raw memory as \a T.
    template <typename T>
    T*
    yyas_ ()
    {
      void *yyp = yybuffer_.yyraw;
      return static_cast<T*> (yyp);
     }

    /// Const accessor to raw memory as \a T.
    template <typename T>
    const T*
    yyas_ () const
    {
      const void *yyp = yybuffer_.yyraw;
      return static_cast<const T*> (yyp);
     }

    union
    {
      /// Strongest alignment constraints.
      long double yyalign_me;
      /// A buffer large enough to store any of the semantic values.
      char yyraw[S];
    } yybuffer_;

    /// Whether the content is built: if defined, the name of the stored type.
    const std::type_info *yytypeid_;
  };


  /// A Bison parser.
  class  parser 
  {
  public:
#ifndef YYSTYPE
    /// An auxiliary type to compute the largest semantic type.
    union union_type
    {
      // "boolv"
      char dummy1[sizeof(bool)];

      // "charv"
      char dummy2[sizeof(char)];

      // "doublev"
      char dummy3[sizeof(double)];

      // "floatv"
      char dummy4[sizeof(float)];

      // "int16_tv"
      char dummy5[sizeof(int16_t)];

      // "int32_tv"
      char dummy6[sizeof(int32_t)];

      // "int64_tv"
      char dummy7[sizeof(int64_t)];

      // "int8_tv"
      char dummy8[sizeof(int8_t)];

      // EDefinition
      char dummy9[sizeof(meat::Definition::sp)];

      // EEnumItem
      char dummy10[sizeof(meat::Enum::Entry::sp)];

      // EEnumeration
      char dummy11[sizeof(meat::Enum::sp)];

      // EExtendField
      // EField
      char dummy12[sizeof(meat::Field::sp)];

      // afile
      char dummy13[sizeof(meat::File::sp)];

      // EImport
      char dummy14[sizeof(meat::Import::sp)];

      // EModule
      char dummy15[sizeof(meat::Module::sp)];

      // ERecord
      char dummy16[sizeof(meat::Record::sp)];

      // EArithType
      // ESufType
      // EScalarType
      char dummy17[sizeof(meat::ScalarType::sp)];

      // ETypeDef
      char dummy18[sizeof(meat::TypeDefinition::sp)];

      // ETypePref
      char dummy19[sizeof(meat::TypePrefix::sp)];

      // ETypeSufQual
      char dummy20[sizeof(meat::TypeSfxQual::sp)];

      // EDefinitions
      char dummy21[sizeof(smunix::sp<meat::Definition::Map>)];

      // EEnumItems
      char dummy22[sizeof(smunix::sp<meat::Enum::Entry::Map>)];

      // EExtendBlock
      // EExtendBlockTail
      // EExtendFields
      // EFieldsBlock
      // EFields
      char dummy23[sizeof(smunix::sp<meat::Field::Map>)];

      // EImportsBlock
      // EImports
      char dummy24[sizeof(smunix::sp<meat::Import::Set>)];

      // EModulesBlock
      char dummy25[sizeof(smunix::sp<meat::Module::Map>)];

      // "id"
      // "stringv"
      // EScopedIds
      // ELabel
      char dummy26[sizeof(std::string)];

      // "uint16_tv"
      // "fixtagv"
      char dummy27[sizeof(uint16_t)];

      // "uint32_tv"
      char dummy28[sizeof(uint32_t)];

      // "uint64_tv"
      char dummy29[sizeof(uint64_t)];

      // "uint8_tv"
      char dummy30[sizeof(uint8_t)];
};

    /// Symbol semantic values.
    typedef variant<sizeof(union_type)> semantic_type;
#else
    typedef YYSTYPE semantic_type;
#endif
    /// Symbol locations.
    typedef location location_type;

    /// Syntax errors thrown from user actions.
    struct syntax_error : std::runtime_error
    {
      syntax_error (const location_type& l, const std::string& m);
      location_type location;
    };

    /// Tokens.
    struct token
    {
      enum yytokentype
      {
        END = 0,
        Module = 258,
        Include = 259,
        Import = 260,
        Imports = 261,
        Where = 262,
        Enum = 263,
        Record = 264,
        Extend = 265,
        Fix = 266,
        Grp = 267,
        Indent = 268,
        As = 269,
        StringType = 270,
        UInt8Type = 271,
        UInt16Type = 272,
        UInt32Type = 273,
        UInt64Type = 274,
        Int8Type = 275,
        Int16Type = 276,
        Int32Type = 277,
        Int64Type = 278,
        BoolType = 279,
        CharType = 280,
        FloatType = 281,
        DoubleType = 282,
        Identifier = 283,
        StringVal = 284,
        UInt8Val = 285,
        UInt16Val = 286,
        UInt32Val = 287,
        UInt64Val = 288,
        Int8Val = 289,
        Int16Val = 290,
        Int32Val = 291,
        Int64Val = 292,
        BoolVal = 293,
        CharVal = 294,
        FloatVal = 295,
        DoubleVal = 296,
        FixTagVal = 297,
        Tag = 298,
        LeftBrace = 299,
        RightBrace = 300,
        LeftBracket = 301,
        RightBracket = 302,
        LeftParen = 303,
        RightParen = 304,
        Pipe = 305,
        Equal = 306,
        IsOfType = 307,
        Colon = 308,
        SemiColon = 309,
        Arobaz = 310,
        Comma = 311,
        Dot = 312
      };
    };

    /// (External) token type, as returned by yylex.
    typedef token::yytokentype token_type;

    /// Symbol type: an internal symbol number.
    typedef int symbol_number_type;

    /// The symbol type number to denote an empty symbol.
    enum { empty_symbol = -2 };

    /// Internal symbol number for tokens (subsumed by symbol_number_type).
    typedef unsigned char token_number_type;

    /// A complete symbol.
    ///
    /// Expects its Base type to provide access to the symbol type
    /// via type_get().
    ///
    /// Provide access to semantic value and location.
    template <typename Base>
    struct basic_symbol : Base
    {
      /// Alias to Base.
      typedef Base super_type;

      /// Default constructor.
      basic_symbol ();

      /// Copy constructor.
      basic_symbol (const basic_symbol& other);

      /// Constructor for valueless symbols, and symbols from each type.

  basic_symbol (typename Base::kind_type t, const location_type& l);

  basic_symbol (typename Base::kind_type t, const bool v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const char v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const double v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const float v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const int16_t v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const int32_t v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const int64_t v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const int8_t v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const meat::Definition::sp v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const meat::Enum::Entry::sp v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const meat::Enum::sp v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const meat::Field::sp v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const meat::File::sp v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const meat::Import::sp v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const meat::Module::sp v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const meat::Record::sp v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const meat::ScalarType::sp v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const meat::TypeDefinition::sp v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const meat::TypePrefix::sp v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const meat::TypeSfxQual::sp v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const smunix::sp<meat::Definition::Map> v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const smunix::sp<meat::Enum::Entry::Map> v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const smunix::sp<meat::Field::Map> v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const smunix::sp<meat::Import::Set> v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const smunix::sp<meat::Module::Map> v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const std::string v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const uint16_t v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const uint32_t v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const uint64_t v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const uint8_t v, const location_type& l);


      /// Constructor for symbols with semantic value.
      basic_symbol (typename Base::kind_type t,
                    const semantic_type& v,
                    const location_type& l);

      /// Destroy the symbol.
      ~basic_symbol ();

      /// Destroy contents, and record that is empty.
      void clear ();

      /// Whether empty.
      bool empty () const;

      /// Destructive move, \a s is emptied into this.
      void move (basic_symbol& s);

      /// The semantic value.
      semantic_type value;

      /// The location.
      location_type location;

    private:
      /// Assignment operator.
      basic_symbol& operator= (const basic_symbol& other);
    };

    /// Type access provider for token (enum) based symbols.
    struct by_type
    {
      /// Default constructor.
      by_type ();

      /// Copy constructor.
      by_type (const by_type& other);

      /// The symbol type as needed by the constructor.
      typedef token_type kind_type;

      /// Constructor from (external) token numbers.
      by_type (kind_type t);

      /// Record that this symbol is empty.
      void clear ();

      /// Steal the symbol type from \a that.
      void move (by_type& that);

      /// The (internal) type number (corresponding to \a type).
      /// \a empty when empty.
      symbol_number_type type_get () const;

      /// The token.
      token_type token () const;

      /// The symbol type.
      /// \a empty_symbol when empty.
      /// An int, not token_number_type, to be able to store empty_symbol.
      int type;
    };

    /// "External" symbols: returned by the scanner.
    typedef basic_symbol<by_type> symbol_type;

    // Symbol constructors declarations.
    static inline
    symbol_type
    make_END (const location_type& l);

    static inline
    symbol_type
    make_Module (const location_type& l);

    static inline
    symbol_type
    make_Include (const location_type& l);

    static inline
    symbol_type
    make_Import (const location_type& l);

    static inline
    symbol_type
    make_Imports (const location_type& l);

    static inline
    symbol_type
    make_Where (const location_type& l);

    static inline
    symbol_type
    make_Enum (const location_type& l);

    static inline
    symbol_type
    make_Record (const location_type& l);

    static inline
    symbol_type
    make_Extend (const location_type& l);

    static inline
    symbol_type
    make_Fix (const location_type& l);

    static inline
    symbol_type
    make_Grp (const location_type& l);

    static inline
    symbol_type
    make_Indent (const location_type& l);

    static inline
    symbol_type
    make_As (const location_type& l);

    static inline
    symbol_type
    make_StringType (const location_type& l);

    static inline
    symbol_type
    make_UInt8Type (const location_type& l);

    static inline
    symbol_type
    make_UInt16Type (const location_type& l);

    static inline
    symbol_type
    make_UInt32Type (const location_type& l);

    static inline
    symbol_type
    make_UInt64Type (const location_type& l);

    static inline
    symbol_type
    make_Int8Type (const location_type& l);

    static inline
    symbol_type
    make_Int16Type (const location_type& l);

    static inline
    symbol_type
    make_Int32Type (const location_type& l);

    static inline
    symbol_type
    make_Int64Type (const location_type& l);

    static inline
    symbol_type
    make_BoolType (const location_type& l);

    static inline
    symbol_type
    make_CharType (const location_type& l);

    static inline
    symbol_type
    make_FloatType (const location_type& l);

    static inline
    symbol_type
    make_DoubleType (const location_type& l);

    static inline
    symbol_type
    make_Identifier (const std::string& v, const location_type& l);

    static inline
    symbol_type
    make_StringVal (const std::string& v, const location_type& l);

    static inline
    symbol_type
    make_UInt8Val (const uint8_t& v, const location_type& l);

    static inline
    symbol_type
    make_UInt16Val (const uint16_t& v, const location_type& l);

    static inline
    symbol_type
    make_UInt32Val (const uint32_t& v, const location_type& l);

    static inline
    symbol_type
    make_UInt64Val (const uint64_t& v, const location_type& l);

    static inline
    symbol_type
    make_Int8Val (const int8_t& v, const location_type& l);

    static inline
    symbol_type
    make_Int16Val (const int16_t& v, const location_type& l);

    static inline
    symbol_type
    make_Int32Val (const int32_t& v, const location_type& l);

    static inline
    symbol_type
    make_Int64Val (const int64_t& v, const location_type& l);

    static inline
    symbol_type
    make_BoolVal (const bool& v, const location_type& l);

    static inline
    symbol_type
    make_CharVal (const char& v, const location_type& l);

    static inline
    symbol_type
    make_FloatVal (const float& v, const location_type& l);

    static inline
    symbol_type
    make_DoubleVal (const double& v, const location_type& l);

    static inline
    symbol_type
    make_FixTagVal (const uint16_t& v, const location_type& l);

    static inline
    symbol_type
    make_Tag (const location_type& l);

    static inline
    symbol_type
    make_LeftBrace (const location_type& l);

    static inline
    symbol_type
    make_RightBrace (const location_type& l);

    static inline
    symbol_type
    make_LeftBracket (const location_type& l);

    static inline
    symbol_type
    make_RightBracket (const location_type& l);

    static inline
    symbol_type
    make_LeftParen (const location_type& l);

    static inline
    symbol_type
    make_RightParen (const location_type& l);

    static inline
    symbol_type
    make_Pipe (const location_type& l);

    static inline
    symbol_type
    make_Equal (const location_type& l);

    static inline
    symbol_type
    make_IsOfType (const location_type& l);

    static inline
    symbol_type
    make_Colon (const location_type& l);

    static inline
    symbol_type
    make_SemiColon (const location_type& l);

    static inline
    symbol_type
    make_Arobaz (const location_type& l);

    static inline
    symbol_type
    make_Comma (const location_type& l);

    static inline
    symbol_type
    make_Dot (const location_type& l);


    /// Build a parser object.
     parser  (meat::driver& drv_yyarg);
    virtual ~ parser  ();

    /// Parse.
    /// \returns  0 iff parsing succeeded.
    virtual int parse ();

#if YYDEBUG
    /// The current debugging stream.
    std::ostream& debug_stream () const YY_ATTRIBUTE_PURE;
    /// Set the current debugging stream.
    void set_debug_stream (std::ostream &);

    /// Type for debugging levels.
    typedef int debug_level_type;
    /// The current debugging level.
    debug_level_type debug_level () const YY_ATTRIBUTE_PURE;
    /// Set the current debugging level.
    void set_debug_level (debug_level_type l);
#endif

    /// Report a syntax error.
    /// \param loc    where the syntax error is found.
    /// \param msg    a description of the syntax error.
    virtual void error (const location_type& loc, const std::string& msg);

    /// Report a syntax error.
    void error (const syntax_error& err);

  private:
    /// This class is not copyable.
     parser  (const  parser &);
     parser & operator= (const  parser &);

    /// State numbers.
    typedef int state_type;

    /// Generate an error message.
    /// \param yystate   the state where the error occurred.
    /// \param yyla      the lookahead token.
    virtual std::string yysyntax_error_ (state_type yystate,
                                         const symbol_type& yyla) const;

    /// Compute post-reduction state.
    /// \param yystate   the current state
    /// \param yysym     the nonterminal to push on the stack
    state_type yy_lr_goto_state_ (state_type yystate, int yysym);

    /// Whether the given \c yypact_ value indicates a defaulted state.
    /// \param yyvalue   the value to check
    static bool yy_pact_value_is_default_ (int yyvalue);

    /// Whether the given \c yytable_ value indicates a syntax error.
    /// \param yyvalue   the value to check
    static bool yy_table_value_is_error_ (int yyvalue);

    static const signed char yypact_ninf_;
    static const signed char yytable_ninf_;

    /// Convert a scanner token number \a t to a symbol number.
    static token_number_type yytranslate_ (token_type t);

    // Tables.
  // YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
  // STATE-NUM.
  static const short int yypact_[];

  // YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
  // Performed when YYTABLE does not specify something else to do.  Zero
  // means the default is an error.
  static const unsigned char yydefact_[];

  // YYPGOTO[NTERM-NUM].
  static const short int yypgoto_[];

  // YYDEFGOTO[NTERM-NUM].
  static const short int yydefgoto_[];

  // YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
  // positive, shift that token.  If negative, reduce the rule whose
  // number is the opposite.  If YYTABLE_NINF, syntax error.
  static const unsigned char yytable_[];

  static const short int yycheck_[];

  // YYSTOS[STATE-NUM] -- The (internal number of the) accessing
  // symbol of state STATE-NUM.
  static const unsigned char yystos_[];

  // YYR1[YYN] -- Symbol number of symbol that rule YYN derives.
  static const unsigned char yyr1_[];

  // YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.
  static const unsigned char yyr2_[];


    /// Convert the symbol name \a n to a form suitable for a diagnostic.
    static std::string yytnamerr_ (const char *n);


    /// For a symbol, its name in clear.
    static const char* const yytname_[];
#if YYDEBUG
  // YYRLINE[YYN] -- Source line where rule number YYN was defined.
  static const unsigned short int yyrline_[];
    /// Report on the debug stream that the rule \a r is going to be reduced.
    virtual void yy_reduce_print_ (int r);
    /// Print the state stack on the debug stream.
    virtual void yystack_print_ ();

    // Debugging.
    int yydebug_;
    std::ostream* yycdebug_;

    /// \brief Display a symbol type, value and location.
    /// \param yyo    The output stream.
    /// \param yysym  The symbol.
    template <typename Base>
    void yy_print_ (std::ostream& yyo, const basic_symbol<Base>& yysym) const;
#endif

    /// \brief Reclaim the memory associated to a symbol.
    /// \param yymsg     Why this token is reclaimed.
    ///                  If null, print nothing.
    /// \param yysym     The symbol.
    template <typename Base>
    void yy_destroy_ (const char* yymsg, basic_symbol<Base>& yysym) const;

  private:
    /// Type access provider for state based symbols.
    struct by_state
    {
      /// Default constructor.
      by_state ();

      /// The symbol type as needed by the constructor.
      typedef state_type kind_type;

      /// Constructor.
      by_state (kind_type s);

      /// Copy constructor.
      by_state (const by_state& other);

      /// Record that this symbol is empty.
      void clear ();

      /// Steal the symbol type from \a that.
      void move (by_state& that);

      /// The (internal) type number (corresponding to \a state).
      /// \a empty_symbol when empty.
      symbol_number_type type_get () const;

      /// The state number used to denote an empty symbol.
      enum { empty_state = -1 };

      /// The state.
      /// \a empty when empty.
      state_type state;
    };

    /// "Internal" symbol: element of the stack.
    struct stack_symbol_type : basic_symbol<by_state>
    {
      /// Superclass.
      typedef basic_symbol<by_state> super_type;
      /// Construct an empty symbol.
      stack_symbol_type ();
      /// Steal the contents from \a sym to build this.
      stack_symbol_type (state_type s, symbol_type& sym);
      /// Assignment, needed by push_back.
      stack_symbol_type& operator= (const stack_symbol_type& that);
    };

    /// Stack type.
    typedef stack<stack_symbol_type> stack_type;

    /// The stack.
    stack_type yystack_;

    /// Push a new state on the stack.
    /// \param m    a debug message to display
    ///             if null, no trace is output.
    /// \param s    the symbol
    /// \warning the contents of \a s.value is stolen.
    void yypush_ (const char* m, stack_symbol_type& s);

    /// Push a new look ahead token on the state on the stack.
    /// \param m    a debug message to display
    ///             if null, no trace is output.
    /// \param s    the state
    /// \param sym  the symbol (for its value and location).
    /// \warning the contents of \a s.value is stolen.
    void yypush_ (const char* m, state_type s, symbol_type& sym);

    /// Pop \a n symbols the three stacks.
    void yypop_ (unsigned int n = 1);

    /// Constants.
    enum
    {
      yyeof_ = 0,
      yylast_ = 186,     ///< Last index in yytable_.
      yynnts_ = 29,  ///< Number of nonterminal symbols.
      yyfinal_ = 5, ///< Termination state number.
      yyterror_ = 1,
      yyerrcode_ = 256,
      yyntokens_ = 58  ///< Number of tokens.
    };


    // User arguments.
    meat::driver& drv;
  };

  // Symbol number corresponding to token number t.
  inline
   parser ::token_number_type
   parser ::yytranslate_ (token_type t)
  {
    static
    const token_number_type
    translate_table[] =
    {
     0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57
    };
    const unsigned int user_token_number_max_ = 312;
    const token_number_type undef_token_ = 2;

    if (static_cast<int>(t) <= yyeof_)
      return yyeof_;
    else if (static_cast<unsigned int> (t) <= user_token_number_max_)
      return translate_table[t];
    else
      return undef_token_;
  }

  inline
   parser ::syntax_error::syntax_error (const location_type& l, const std::string& m)
    : std::runtime_error (m)
    , location (l)
  {}

  // basic_symbol.
  template <typename Base>
  inline
   parser ::basic_symbol<Base>::basic_symbol ()
    : value ()
  {}

  template <typename Base>
  inline
   parser ::basic_symbol<Base>::basic_symbol (const basic_symbol& other)
    : Base (other)
    , value ()
    , location (other.location)
  {
      switch (other.type_get ())
    {
      case 38: // "boolv"
        value.copy< bool > (other.value);
        break;

      case 39: // "charv"
        value.copy< char > (other.value);
        break;

      case 41: // "doublev"
        value.copy< double > (other.value);
        break;

      case 40: // "floatv"
        value.copy< float > (other.value);
        break;

      case 35: // "int16_tv"
        value.copy< int16_t > (other.value);
        break;

      case 36: // "int32_tv"
        value.copy< int32_t > (other.value);
        break;

      case 37: // "int64_tv"
        value.copy< int64_t > (other.value);
        break;

      case 34: // "int8_tv"
        value.copy< int8_t > (other.value);
        break;

      case 67: // EDefinition
        value.copy< meat::Definition::sp > (other.value);
        break;

      case 70: // EEnumItem
        value.copy< meat::Enum::Entry::sp > (other.value);
        break;

      case 68: // EEnumeration
        value.copy< meat::Enum::sp > (other.value);
        break;

      case 75: // EExtendField
      case 79: // EField
        value.copy< meat::Field::sp > (other.value);
        break;

      case 59: // afile
        value.copy< meat::File::sp > (other.value);
        break;

      case 62: // EImport
        value.copy< meat::Import::sp > (other.value);
        break;

      case 65: // EModule
        value.copy< meat::Module::sp > (other.value);
        break;

      case 71: // ERecord
        value.copy< meat::Record::sp > (other.value);
        break;

      case 82: // EArithType
      case 83: // ESufType
      case 84: // EScalarType
        value.copy< meat::ScalarType::sp > (other.value);
        break;

      case 80: // ETypeDef
        value.copy< meat::TypeDefinition::sp > (other.value);
        break;

      case 81: // ETypePref
        value.copy< meat::TypePrefix::sp > (other.value);
        break;

      case 85: // ETypeSufQual
        value.copy< meat::TypeSfxQual::sp > (other.value);
        break;

      case 66: // EDefinitions
        value.copy< smunix::sp<meat::Definition::Map> > (other.value);
        break;

      case 69: // EEnumItems
        value.copy< smunix::sp<meat::Enum::Entry::Map> > (other.value);
        break;

      case 72: // EExtendBlock
      case 73: // EExtendBlockTail
      case 74: // EExtendFields
      case 77: // EFieldsBlock
      case 78: // EFields
        value.copy< smunix::sp<meat::Field::Map> > (other.value);
        break;

      case 60: // EImportsBlock
      case 61: // EImports
        value.copy< smunix::sp<meat::Import::Set> > (other.value);
        break;

      case 63: // EModulesBlock
        value.copy< smunix::sp<meat::Module::Map> > (other.value);
        break;

      case 28: // "id"
      case 29: // "stringv"
      case 76: // EScopedIds
      case 86: // ELabel
        value.copy< std::string > (other.value);
        break;

      case 31: // "uint16_tv"
      case 42: // "fixtagv"
        value.copy< uint16_t > (other.value);
        break;

      case 32: // "uint32_tv"
        value.copy< uint32_t > (other.value);
        break;

      case 33: // "uint64_tv"
        value.copy< uint64_t > (other.value);
        break;

      case 30: // "uint8_tv"
        value.copy< uint8_t > (other.value);
        break;

      default:
        break;
    }

  }


  template <typename Base>
  inline
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const semantic_type& v, const location_type& l)
    : Base (t)
    , value ()
    , location (l)
  {
    (void) v;
      switch (this->type_get ())
    {
      case 38: // "boolv"
        value.copy< bool > (v);
        break;

      case 39: // "charv"
        value.copy< char > (v);
        break;

      case 41: // "doublev"
        value.copy< double > (v);
        break;

      case 40: // "floatv"
        value.copy< float > (v);
        break;

      case 35: // "int16_tv"
        value.copy< int16_t > (v);
        break;

      case 36: // "int32_tv"
        value.copy< int32_t > (v);
        break;

      case 37: // "int64_tv"
        value.copy< int64_t > (v);
        break;

      case 34: // "int8_tv"
        value.copy< int8_t > (v);
        break;

      case 67: // EDefinition
        value.copy< meat::Definition::sp > (v);
        break;

      case 70: // EEnumItem
        value.copy< meat::Enum::Entry::sp > (v);
        break;

      case 68: // EEnumeration
        value.copy< meat::Enum::sp > (v);
        break;

      case 75: // EExtendField
      case 79: // EField
        value.copy< meat::Field::sp > (v);
        break;

      case 59: // afile
        value.copy< meat::File::sp > (v);
        break;

      case 62: // EImport
        value.copy< meat::Import::sp > (v);
        break;

      case 65: // EModule
        value.copy< meat::Module::sp > (v);
        break;

      case 71: // ERecord
        value.copy< meat::Record::sp > (v);
        break;

      case 82: // EArithType
      case 83: // ESufType
      case 84: // EScalarType
        value.copy< meat::ScalarType::sp > (v);
        break;

      case 80: // ETypeDef
        value.copy< meat::TypeDefinition::sp > (v);
        break;

      case 81: // ETypePref
        value.copy< meat::TypePrefix::sp > (v);
        break;

      case 85: // ETypeSufQual
        value.copy< meat::TypeSfxQual::sp > (v);
        break;

      case 66: // EDefinitions
        value.copy< smunix::sp<meat::Definition::Map> > (v);
        break;

      case 69: // EEnumItems
        value.copy< smunix::sp<meat::Enum::Entry::Map> > (v);
        break;

      case 72: // EExtendBlock
      case 73: // EExtendBlockTail
      case 74: // EExtendFields
      case 77: // EFieldsBlock
      case 78: // EFields
        value.copy< smunix::sp<meat::Field::Map> > (v);
        break;

      case 60: // EImportsBlock
      case 61: // EImports
        value.copy< smunix::sp<meat::Import::Set> > (v);
        break;

      case 63: // EModulesBlock
        value.copy< smunix::sp<meat::Module::Map> > (v);
        break;

      case 28: // "id"
      case 29: // "stringv"
      case 76: // EScopedIds
      case 86: // ELabel
        value.copy< std::string > (v);
        break;

      case 31: // "uint16_tv"
      case 42: // "fixtagv"
        value.copy< uint16_t > (v);
        break;

      case 32: // "uint32_tv"
        value.copy< uint32_t > (v);
        break;

      case 33: // "uint64_tv"
        value.copy< uint64_t > (v);
        break;

      case 30: // "uint8_tv"
        value.copy< uint8_t > (v);
        break;

      default:
        break;
    }
}


  // Implementation of basic_symbol constructor for each type.

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const location_type& l)
    : Base (t)
    , value ()
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const bool v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const char v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const double v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const float v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const int16_t v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const int32_t v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const int64_t v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const int8_t v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const meat::Definition::sp v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const meat::Enum::Entry::sp v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const meat::Enum::sp v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const meat::Field::sp v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const meat::File::sp v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const meat::Import::sp v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const meat::Module::sp v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const meat::Record::sp v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const meat::ScalarType::sp v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const meat::TypeDefinition::sp v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const meat::TypePrefix::sp v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const meat::TypeSfxQual::sp v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const smunix::sp<meat::Definition::Map> v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const smunix::sp<meat::Enum::Entry::Map> v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const smunix::sp<meat::Field::Map> v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const smunix::sp<meat::Import::Set> v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const smunix::sp<meat::Module::Map> v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const std::string v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const uint16_t v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const uint32_t v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const uint64_t v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const uint8_t v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}


  template <typename Base>
  inline
   parser ::basic_symbol<Base>::~basic_symbol ()
  {
    clear ();
  }

  template <typename Base>
  inline
  void
   parser ::basic_symbol<Base>::clear ()
  {
    // User destructor.
    symbol_number_type yytype = this->type_get ();
    basic_symbol<Base>& yysym = *this;
    (void) yysym;
    switch (yytype)
    {
   default:
      break;
    }

    // Type destructor.
    switch (yytype)
    {
      case 38: // "boolv"
        value.template destroy< bool > ();
        break;

      case 39: // "charv"
        value.template destroy< char > ();
        break;

      case 41: // "doublev"
        value.template destroy< double > ();
        break;

      case 40: // "floatv"
        value.template destroy< float > ();
        break;

      case 35: // "int16_tv"
        value.template destroy< int16_t > ();
        break;

      case 36: // "int32_tv"
        value.template destroy< int32_t > ();
        break;

      case 37: // "int64_tv"
        value.template destroy< int64_t > ();
        break;

      case 34: // "int8_tv"
        value.template destroy< int8_t > ();
        break;

      case 67: // EDefinition
        value.template destroy< meat::Definition::sp > ();
        break;

      case 70: // EEnumItem
        value.template destroy< meat::Enum::Entry::sp > ();
        break;

      case 68: // EEnumeration
        value.template destroy< meat::Enum::sp > ();
        break;

      case 75: // EExtendField
      case 79: // EField
        value.template destroy< meat::Field::sp > ();
        break;

      case 59: // afile
        value.template destroy< meat::File::sp > ();
        break;

      case 62: // EImport
        value.template destroy< meat::Import::sp > ();
        break;

      case 65: // EModule
        value.template destroy< meat::Module::sp > ();
        break;

      case 71: // ERecord
        value.template destroy< meat::Record::sp > ();
        break;

      case 82: // EArithType
      case 83: // ESufType
      case 84: // EScalarType
        value.template destroy< meat::ScalarType::sp > ();
        break;

      case 80: // ETypeDef
        value.template destroy< meat::TypeDefinition::sp > ();
        break;

      case 81: // ETypePref
        value.template destroy< meat::TypePrefix::sp > ();
        break;

      case 85: // ETypeSufQual
        value.template destroy< meat::TypeSfxQual::sp > ();
        break;

      case 66: // EDefinitions
        value.template destroy< smunix::sp<meat::Definition::Map> > ();
        break;

      case 69: // EEnumItems
        value.template destroy< smunix::sp<meat::Enum::Entry::Map> > ();
        break;

      case 72: // EExtendBlock
      case 73: // EExtendBlockTail
      case 74: // EExtendFields
      case 77: // EFieldsBlock
      case 78: // EFields
        value.template destroy< smunix::sp<meat::Field::Map> > ();
        break;

      case 60: // EImportsBlock
      case 61: // EImports
        value.template destroy< smunix::sp<meat::Import::Set> > ();
        break;

      case 63: // EModulesBlock
        value.template destroy< smunix::sp<meat::Module::Map> > ();
        break;

      case 28: // "id"
      case 29: // "stringv"
      case 76: // EScopedIds
      case 86: // ELabel
        value.template destroy< std::string > ();
        break;

      case 31: // "uint16_tv"
      case 42: // "fixtagv"
        value.template destroy< uint16_t > ();
        break;

      case 32: // "uint32_tv"
        value.template destroy< uint32_t > ();
        break;

      case 33: // "uint64_tv"
        value.template destroy< uint64_t > ();
        break;

      case 30: // "uint8_tv"
        value.template destroy< uint8_t > ();
        break;

      default:
        break;
    }

    Base::clear ();
  }

  template <typename Base>
  inline
  bool
   parser ::basic_symbol<Base>::empty () const
  {
    return Base::type_get () == empty_symbol;
  }

  template <typename Base>
  inline
  void
   parser ::basic_symbol<Base>::move (basic_symbol& s)
  {
    super_type::move(s);
      switch (this->type_get ())
    {
      case 38: // "boolv"
        value.move< bool > (s.value);
        break;

      case 39: // "charv"
        value.move< char > (s.value);
        break;

      case 41: // "doublev"
        value.move< double > (s.value);
        break;

      case 40: // "floatv"
        value.move< float > (s.value);
        break;

      case 35: // "int16_tv"
        value.move< int16_t > (s.value);
        break;

      case 36: // "int32_tv"
        value.move< int32_t > (s.value);
        break;

      case 37: // "int64_tv"
        value.move< int64_t > (s.value);
        break;

      case 34: // "int8_tv"
        value.move< int8_t > (s.value);
        break;

      case 67: // EDefinition
        value.move< meat::Definition::sp > (s.value);
        break;

      case 70: // EEnumItem
        value.move< meat::Enum::Entry::sp > (s.value);
        break;

      case 68: // EEnumeration
        value.move< meat::Enum::sp > (s.value);
        break;

      case 75: // EExtendField
      case 79: // EField
        value.move< meat::Field::sp > (s.value);
        break;

      case 59: // afile
        value.move< meat::File::sp > (s.value);
        break;

      case 62: // EImport
        value.move< meat::Import::sp > (s.value);
        break;

      case 65: // EModule
        value.move< meat::Module::sp > (s.value);
        break;

      case 71: // ERecord
        value.move< meat::Record::sp > (s.value);
        break;

      case 82: // EArithType
      case 83: // ESufType
      case 84: // EScalarType
        value.move< meat::ScalarType::sp > (s.value);
        break;

      case 80: // ETypeDef
        value.move< meat::TypeDefinition::sp > (s.value);
        break;

      case 81: // ETypePref
        value.move< meat::TypePrefix::sp > (s.value);
        break;

      case 85: // ETypeSufQual
        value.move< meat::TypeSfxQual::sp > (s.value);
        break;

      case 66: // EDefinitions
        value.move< smunix::sp<meat::Definition::Map> > (s.value);
        break;

      case 69: // EEnumItems
        value.move< smunix::sp<meat::Enum::Entry::Map> > (s.value);
        break;

      case 72: // EExtendBlock
      case 73: // EExtendBlockTail
      case 74: // EExtendFields
      case 77: // EFieldsBlock
      case 78: // EFields
        value.move< smunix::sp<meat::Field::Map> > (s.value);
        break;

      case 60: // EImportsBlock
      case 61: // EImports
        value.move< smunix::sp<meat::Import::Set> > (s.value);
        break;

      case 63: // EModulesBlock
        value.move< smunix::sp<meat::Module::Map> > (s.value);
        break;

      case 28: // "id"
      case 29: // "stringv"
      case 76: // EScopedIds
      case 86: // ELabel
        value.move< std::string > (s.value);
        break;

      case 31: // "uint16_tv"
      case 42: // "fixtagv"
        value.move< uint16_t > (s.value);
        break;

      case 32: // "uint32_tv"
        value.move< uint32_t > (s.value);
        break;

      case 33: // "uint64_tv"
        value.move< uint64_t > (s.value);
        break;

      case 30: // "uint8_tv"
        value.move< uint8_t > (s.value);
        break;

      default:
        break;
    }

    location = s.location;
  }

  // by_type.
  inline
   parser ::by_type::by_type ()
    : type (empty_symbol)
  {}

  inline
   parser ::by_type::by_type (const by_type& other)
    : type (other.type)
  {}

  inline
   parser ::by_type::by_type (token_type t)
    : type (yytranslate_ (t))
  {}

  inline
  void
   parser ::by_type::clear ()
  {
    type = empty_symbol;
  }

  inline
  void
   parser ::by_type::move (by_type& that)
  {
    type = that.type;
    that.clear ();
  }

  inline
  int
   parser ::by_type::type_get () const
  {
    return type;
  }

  inline
   parser ::token_type
   parser ::by_type::token () const
  {
    // YYTOKNUM[NUM] -- (External) token number corresponding to the
    // (internal) symbol number NUM (which must be that of a token).  */
    static
    const unsigned short int
    yytoken_number_[] =
    {
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312
    };
    return static_cast<token_type> (yytoken_number_[type]);
  }
  // Implementation of make_symbol for each symbol type.
   parser ::symbol_type
   parser ::make_END (const location_type& l)
  {
    return symbol_type (token::END, l);
  }

   parser ::symbol_type
   parser ::make_Module (const location_type& l)
  {
    return symbol_type (token::Module, l);
  }

   parser ::symbol_type
   parser ::make_Include (const location_type& l)
  {
    return symbol_type (token::Include, l);
  }

   parser ::symbol_type
   parser ::make_Import (const location_type& l)
  {
    return symbol_type (token::Import, l);
  }

   parser ::symbol_type
   parser ::make_Imports (const location_type& l)
  {
    return symbol_type (token::Imports, l);
  }

   parser ::symbol_type
   parser ::make_Where (const location_type& l)
  {
    return symbol_type (token::Where, l);
  }

   parser ::symbol_type
   parser ::make_Enum (const location_type& l)
  {
    return symbol_type (token::Enum, l);
  }

   parser ::symbol_type
   parser ::make_Record (const location_type& l)
  {
    return symbol_type (token::Record, l);
  }

   parser ::symbol_type
   parser ::make_Extend (const location_type& l)
  {
    return symbol_type (token::Extend, l);
  }

   parser ::symbol_type
   parser ::make_Fix (const location_type& l)
  {
    return symbol_type (token::Fix, l);
  }

   parser ::symbol_type
   parser ::make_Grp (const location_type& l)
  {
    return symbol_type (token::Grp, l);
  }

   parser ::symbol_type
   parser ::make_Indent (const location_type& l)
  {
    return symbol_type (token::Indent, l);
  }

   parser ::symbol_type
   parser ::make_As (const location_type& l)
  {
    return symbol_type (token::As, l);
  }

   parser ::symbol_type
   parser ::make_StringType (const location_type& l)
  {
    return symbol_type (token::StringType, l);
  }

   parser ::symbol_type
   parser ::make_UInt8Type (const location_type& l)
  {
    return symbol_type (token::UInt8Type, l);
  }

   parser ::symbol_type
   parser ::make_UInt16Type (const location_type& l)
  {
    return symbol_type (token::UInt16Type, l);
  }

   parser ::symbol_type
   parser ::make_UInt32Type (const location_type& l)
  {
    return symbol_type (token::UInt32Type, l);
  }

   parser ::symbol_type
   parser ::make_UInt64Type (const location_type& l)
  {
    return symbol_type (token::UInt64Type, l);
  }

   parser ::symbol_type
   parser ::make_Int8Type (const location_type& l)
  {
    return symbol_type (token::Int8Type, l);
  }

   parser ::symbol_type
   parser ::make_Int16Type (const location_type& l)
  {
    return symbol_type (token::Int16Type, l);
  }

   parser ::symbol_type
   parser ::make_Int32Type (const location_type& l)
  {
    return symbol_type (token::Int32Type, l);
  }

   parser ::symbol_type
   parser ::make_Int64Type (const location_type& l)
  {
    return symbol_type (token::Int64Type, l);
  }

   parser ::symbol_type
   parser ::make_BoolType (const location_type& l)
  {
    return symbol_type (token::BoolType, l);
  }

   parser ::symbol_type
   parser ::make_CharType (const location_type& l)
  {
    return symbol_type (token::CharType, l);
  }

   parser ::symbol_type
   parser ::make_FloatType (const location_type& l)
  {
    return symbol_type (token::FloatType, l);
  }

   parser ::symbol_type
   parser ::make_DoubleType (const location_type& l)
  {
    return symbol_type (token::DoubleType, l);
  }

   parser ::symbol_type
   parser ::make_Identifier (const std::string& v, const location_type& l)
  {
    return symbol_type (token::Identifier, v, l);
  }

   parser ::symbol_type
   parser ::make_StringVal (const std::string& v, const location_type& l)
  {
    return symbol_type (token::StringVal, v, l);
  }

   parser ::symbol_type
   parser ::make_UInt8Val (const uint8_t& v, const location_type& l)
  {
    return symbol_type (token::UInt8Val, v, l);
  }

   parser ::symbol_type
   parser ::make_UInt16Val (const uint16_t& v, const location_type& l)
  {
    return symbol_type (token::UInt16Val, v, l);
  }

   parser ::symbol_type
   parser ::make_UInt32Val (const uint32_t& v, const location_type& l)
  {
    return symbol_type (token::UInt32Val, v, l);
  }

   parser ::symbol_type
   parser ::make_UInt64Val (const uint64_t& v, const location_type& l)
  {
    return symbol_type (token::UInt64Val, v, l);
  }

   parser ::symbol_type
   parser ::make_Int8Val (const int8_t& v, const location_type& l)
  {
    return symbol_type (token::Int8Val, v, l);
  }

   parser ::symbol_type
   parser ::make_Int16Val (const int16_t& v, const location_type& l)
  {
    return symbol_type (token::Int16Val, v, l);
  }

   parser ::symbol_type
   parser ::make_Int32Val (const int32_t& v, const location_type& l)
  {
    return symbol_type (token::Int32Val, v, l);
  }

   parser ::symbol_type
   parser ::make_Int64Val (const int64_t& v, const location_type& l)
  {
    return symbol_type (token::Int64Val, v, l);
  }

   parser ::symbol_type
   parser ::make_BoolVal (const bool& v, const location_type& l)
  {
    return symbol_type (token::BoolVal, v, l);
  }

   parser ::symbol_type
   parser ::make_CharVal (const char& v, const location_type& l)
  {
    return symbol_type (token::CharVal, v, l);
  }

   parser ::symbol_type
   parser ::make_FloatVal (const float& v, const location_type& l)
  {
    return symbol_type (token::FloatVal, v, l);
  }

   parser ::symbol_type
   parser ::make_DoubleVal (const double& v, const location_type& l)
  {
    return symbol_type (token::DoubleVal, v, l);
  }

   parser ::symbol_type
   parser ::make_FixTagVal (const uint16_t& v, const location_type& l)
  {
    return symbol_type (token::FixTagVal, v, l);
  }

   parser ::symbol_type
   parser ::make_Tag (const location_type& l)
  {
    return symbol_type (token::Tag, l);
  }

   parser ::symbol_type
   parser ::make_LeftBrace (const location_type& l)
  {
    return symbol_type (token::LeftBrace, l);
  }

   parser ::symbol_type
   parser ::make_RightBrace (const location_type& l)
  {
    return symbol_type (token::RightBrace, l);
  }

   parser ::symbol_type
   parser ::make_LeftBracket (const location_type& l)
  {
    return symbol_type (token::LeftBracket, l);
  }

   parser ::symbol_type
   parser ::make_RightBracket (const location_type& l)
  {
    return symbol_type (token::RightBracket, l);
  }

   parser ::symbol_type
   parser ::make_LeftParen (const location_type& l)
  {
    return symbol_type (token::LeftParen, l);
  }

   parser ::symbol_type
   parser ::make_RightParen (const location_type& l)
  {
    return symbol_type (token::RightParen, l);
  }

   parser ::symbol_type
   parser ::make_Pipe (const location_type& l)
  {
    return symbol_type (token::Pipe, l);
  }

   parser ::symbol_type
   parser ::make_Equal (const location_type& l)
  {
    return symbol_type (token::Equal, l);
  }

   parser ::symbol_type
   parser ::make_IsOfType (const location_type& l)
  {
    return symbol_type (token::IsOfType, l);
  }

   parser ::symbol_type
   parser ::make_Colon (const location_type& l)
  {
    return symbol_type (token::Colon, l);
  }

   parser ::symbol_type
   parser ::make_SemiColon (const location_type& l)
  {
    return symbol_type (token::SemiColon, l);
  }

   parser ::symbol_type
   parser ::make_Arobaz (const location_type& l)
  {
    return symbol_type (token::Arobaz, l);
  }

   parser ::symbol_type
   parser ::make_Comma (const location_type& l)
  {
    return symbol_type (token::Comma, l);
  }

   parser ::symbol_type
   parser ::make_Dot (const location_type& l)
  {
    return symbol_type (token::Dot, l);
  }


#line 13 "pgen/parser.y" // lalr1.cc:392
} } //  meat::yy 
#line 2379 "src/meat/model/parser.hh" // lalr1.cc:392




#endif // !YY_YY_SRC_MEAT_MODEL_PARSER_HH_INCLUDED
