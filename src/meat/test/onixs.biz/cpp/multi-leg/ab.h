
#pragma once

/*
File Generated from : src/meat/test/onixs.biz/meat/multi-leg/ab.meat
Author : Providence Salumu <Providence.Salumu@smunix.com>
File Generated from : src/meat/test/onixs.biz/meat/multi-leg/ab.meat
using "build/scratch/grill/grill src/meat/test/onixs.biz/meat/multi-leg/ab.meat &> src/meat/test/onixs.biz/meat/multi-leg/ab.h"
Copyright 2015 SMUNIX Inc.
*/

#include <type_traits>
#include <smunix/core-module.hh>

namespace types {
  namespace s = smunix;
  enum class Type {
    AB = 1,
  };
  enum class SwapType {
    Bear = 1,
  };
  enum class Side {
    Buy = 1,
    Sell = 2,
    BuyMinus = 3,
    SellPlus = 4,
    SellShort = 5,
  };
  struct Date {
    s::data<uint8_t> _yy;
    s::data<uint8_t> _mm;
    s::data<uint8_t> _dd;
    
    // all accessors - get
    uint8_t& yy() { return *_yy; }
    uint8_t const& yy() const { return *_yy; }
    uint8_t& mm() { return *_mm; }
    uint8_t const& mm() const { return *_mm; }
    uint8_t& dd() { return *_dd; }
    uint8_t const& dd() const { return *_dd; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      Date lDate;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<uint8_t>("yy", s::p<uint8_t>(s::addr(lDate.yy())) - s::p<uint8_t>(s::addr(lDate)), std::forward<T>(t));
      Describe::template apply<uint8_t>("mm", s::p<uint8_t>(s::addr(lDate.mm())) - s::p<uint8_t>(s::addr(lDate)), std::forward<T>(t));
      Describe::template apply<uint8_t>("dd", s::p<uint8_t>(s::addr(lDate.dd())) - s::p<uint8_t>(s::addr(lDate)), std::forward<T>(t));
    }
  };
  struct Time {
    s::data<uint8_t> _hh;
    s::data<uint8_t> _mm;
    s::data<uint8_t> _ss;
    
    // all accessors - get
    uint8_t& hh() { return *_hh; }
    uint8_t const& hh() const { return *_hh; }
    uint8_t& mm() { return *_mm; }
    uint8_t const& mm() const { return *_mm; }
    uint8_t& ss() { return *_ss; }
    uint8_t const& ss() const { return *_ss; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      Time lTime;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<uint8_t>("hh", s::p<uint8_t>(s::addr(lTime.hh())) - s::p<uint8_t>(s::addr(lTime)), std::forward<T>(t));
      Describe::template apply<uint8_t>("mm", s::p<uint8_t>(s::addr(lTime.mm())) - s::p<uint8_t>(s::addr(lTime)), std::forward<T>(t));
      Describe::template apply<uint8_t>("ss", s::p<uint8_t>(s::addr(lTime.ss())) - s::p<uint8_t>(s::addr(lTime)), std::forward<T>(t));
    }
  };
  struct Datetime {
    s::data<Date> _date;
    s::data<Time> _time;
    
    // all accessors - get
    Date& date() { return *_date; }
    Date const& date() const { return *_date; }
    Time& time() { return *_time; }
    Time const& time() const { return *_time; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      Datetime lDatetime;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<Date>("date", s::p<uint8_t>(s::addr(lDatetime.date())) - s::p<uint8_t>(s::addr(lDatetime)), std::forward<T>(t));
      Describe::template apply<Time>("time", s::p<uint8_t>(s::addr(lDatetime.time())) - s::p<uint8_t>(s::addr(lDatetime)), std::forward<T>(t));
    }
  };
  struct Id {
    s::data<size_t> _value;
    
    // all accessors - get
    size_t& value() { return *_value; }
    size_t const& value() const { return *_value; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      Id lId;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<size_t>("value", s::p<uint8_t>(s::addr(lId.value())) - s::p<uint8_t>(s::addr(lId)), std::forward<T>(t));
    }
  };
  struct Qty {
    s::data<size_t> _value;
    
    // all accessors - get
    size_t& value() { return *_value; }
    size_t const& value() const { return *_value; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      Qty lQty;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<size_t>("value", s::p<uint8_t>(s::addr(lQty.value())) - s::p<uint8_t>(s::addr(lQty)), std::forward<T>(t));
    }
  };
} // namespace types

namespace onixs {
  namespace s = smunix;
  struct Instrument {
    s::data<s::off::Ref<s::dyn::arr<char>>> _Symbol;
    s::data<s::off::Ref<s::dyn::arr<char>>> _SymbolSfx;
    s::data<s::off::Ref<types::Date>> _MaturityData;
    
    // all accessors - get
    s::off::Ref<s::dyn::arr<char>>& Symbol() { return *_Symbol; }
    s::off::Ref<s::dyn::arr<char>> const& Symbol() const { return *_Symbol; }
    s::off::Ref<s::dyn::arr<char>>& SymbolSfx() { return *_SymbolSfx; }
    s::off::Ref<s::dyn::arr<char>> const& SymbolSfx() const { return *_SymbolSfx; }
    s::off::Ref<types::Date>& MaturityData() { return *_MaturityData; }
    s::off::Ref<types::Date> const& MaturityData() const { return *_MaturityData; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      Instrument lInstrument;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<s::off::Ref<s::dyn::arr<char>>>("Symbol", s::p<uint8_t>(s::addr(lInstrument.Symbol())) - s::p<uint8_t>(s::addr(lInstrument)), std::forward<T>(t));
      Describe::template apply<s::off::Ref<s::dyn::arr<char>>>("SymbolSfx", s::p<uint8_t>(s::addr(lInstrument.SymbolSfx())) - s::p<uint8_t>(s::addr(lInstrument)), std::forward<T>(t));
      Describe::template apply<s::off::Ref<types::Date>>("MaturityData", s::p<uint8_t>(s::addr(lInstrument.MaturityData())) - s::p<uint8_t>(s::addr(lInstrument)), std::forward<T>(t));
    }
    
    // setter
    template<class T> void set(bool& success, uint16_t n, T&& t) {
      using SetTag = s::tag::Set<typename std::decay<T>::type>;
      using SetGrp = s::grp::Set<typename std::decay<T>::type>;
      switch(n) {
        case 55: SetTag::template apply(success, Symbol(), std::forward<T>(t)); return;
        case 65: SetTag::template apply(success, SymbolSfx(), std::forward<T>(t)); return;
        case 541: SetTag::template apply(success, MaturityData(), std::forward<T>(t)); return;
        default: success = false; return;
      }
    }
    
    // getter
    template<class T> void get(bool& success, T&& t) {
      using GetTag = s::tag::Get<typename std::decay<T>::type>;
      using GetGrp = s::grp::Get<typename std::decay<T>::type>;
      GetTag::template apply(success, Symbol(), 55, std::forward<T>(t));
      GetTag::template apply(success, SymbolSfx(), 65, std::forward<T>(t));
      GetTag::template apply(success, MaturityData(), 541, std::forward<T>(t));
    }
  };
  struct InstrumentLeg {
    s::data<s::off::Ref<s::dyn::arr<char>>> _LegSymbol;
    s::data<s::off::Ref<s::dyn::arr<char>>> _LegSymbolSfx;
    s::data<s::off::Ref<types::Date>> _LegMaturityData;
    
    // all accessors - get
    s::off::Ref<s::dyn::arr<char>>& LegSymbol() { return *_LegSymbol; }
    s::off::Ref<s::dyn::arr<char>> const& LegSymbol() const { return *_LegSymbol; }
    s::off::Ref<s::dyn::arr<char>>& LegSymbolSfx() { return *_LegSymbolSfx; }
    s::off::Ref<s::dyn::arr<char>> const& LegSymbolSfx() const { return *_LegSymbolSfx; }
    s::off::Ref<types::Date>& LegMaturityData() { return *_LegMaturityData; }
    s::off::Ref<types::Date> const& LegMaturityData() const { return *_LegMaturityData; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      InstrumentLeg lInstrumentLeg;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<s::off::Ref<s::dyn::arr<char>>>("LegSymbol", s::p<uint8_t>(s::addr(lInstrumentLeg.LegSymbol())) - s::p<uint8_t>(s::addr(lInstrumentLeg)), std::forward<T>(t));
      Describe::template apply<s::off::Ref<s::dyn::arr<char>>>("LegSymbolSfx", s::p<uint8_t>(s::addr(lInstrumentLeg.LegSymbolSfx())) - s::p<uint8_t>(s::addr(lInstrumentLeg)), std::forward<T>(t));
      Describe::template apply<s::off::Ref<types::Date>>("LegMaturityData", s::p<uint8_t>(s::addr(lInstrumentLeg.LegMaturityData())) - s::p<uint8_t>(s::addr(lInstrumentLeg)), std::forward<T>(t));
    }
    
    // setter
    template<class T> void set(bool& success, uint16_t n, T&& t) {
      using SetTag = s::tag::Set<typename std::decay<T>::type>;
      using SetGrp = s::grp::Set<typename std::decay<T>::type>;
      switch(n) {
        case 600: SetTag::template apply(success, LegSymbol(), std::forward<T>(t)); return;
        case 601: SetTag::template apply(success, LegSymbolSfx(), std::forward<T>(t)); return;
        case 611: SetTag::template apply(success, LegMaturityData(), std::forward<T>(t)); return;
        default: success = false; return;
      }
    }
    
    // getter
    template<class T> void get(bool& success, T&& t) {
      using GetTag = s::tag::Get<typename std::decay<T>::type>;
      using GetGrp = s::grp::Get<typename std::decay<T>::type>;
      GetTag::template apply(success, LegSymbol(), 600, std::forward<T>(t));
      GetTag::template apply(success, LegSymbolSfx(), 601, std::forward<T>(t));
      GetTag::template apply(success, LegMaturityData(), 611, std::forward<T>(t));
    }
  };
  struct InstrumentLegBlock {
    s::data<InstrumentLeg> _Leg;
    s::data<types::Qty> _LegQty;
    s::data<types::SwapType> _LegSwapType;
    s::data<types::Side> _LegSide;
    
    // all accessors - get
    InstrumentLeg& Leg() { return *_Leg; }
    InstrumentLeg const& Leg() const { return *_Leg; }
    types::Qty& LegQty() { return *_LegQty; }
    types::Qty const& LegQty() const { return *_LegQty; }
    types::SwapType& LegSwapType() { return *_LegSwapType; }
    types::SwapType const& LegSwapType() const { return *_LegSwapType; }
    types::Side& LegSide() { return *_LegSide; }
    types::Side const& LegSide() const { return *_LegSide; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      InstrumentLegBlock lInstrumentLegBlock;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<InstrumentLeg>("Leg", s::p<uint8_t>(s::addr(lInstrumentLegBlock.Leg())) - s::p<uint8_t>(s::addr(lInstrumentLegBlock)), std::forward<T>(t));
      Describe::template apply<types::Qty>("LegQty", s::p<uint8_t>(s::addr(lInstrumentLegBlock.LegQty())) - s::p<uint8_t>(s::addr(lInstrumentLegBlock)), std::forward<T>(t));
      Describe::template apply<types::SwapType>("LegSwapType", s::p<uint8_t>(s::addr(lInstrumentLegBlock.LegSwapType())) - s::p<uint8_t>(s::addr(lInstrumentLegBlock)), std::forward<T>(t));
      Describe::template apply<types::Side>("LegSide", s::p<uint8_t>(s::addr(lInstrumentLegBlock.LegSide())) - s::p<uint8_t>(s::addr(lInstrumentLegBlock)), std::forward<T>(t));
    }
    
    // setter
    template<class T> void set(bool& success, uint16_t n, T&& t) {
      using SetTag = s::tag::Set<typename std::decay<T>::type>;
      using SetGrp = s::grp::Set<typename std::decay<T>::type>;
      switch(n) {
        case 687: SetTag::template apply(success, LegQty(), std::forward<T>(t)); return;
        case 690: SetTag::template apply(success, LegSwapType(), std::forward<T>(t)); return;
        case 624: SetTag::template apply(success, LegSide(), std::forward<T>(t)); return;
        default: success = false; return;
      }
    }
    
    // getter
    template<class T> void get(bool& success, T&& t) {
      using GetTag = s::tag::Get<typename std::decay<T>::type>;
      using GetGrp = s::grp::Get<typename std::decay<T>::type>;
      GetTag::template apply(success, LegQty(), 687, std::forward<T>(t));
      GetTag::template apply(success, LegSwapType(), 690, std::forward<T>(t));
      GetTag::template apply(success, LegSide(), 624, std::forward<T>(t));
    }
  };
  struct NewOrderMultileg {
    s::data<types::Type> _MsgType;
    s::data<types::Id> _ClOrdId;
    s::data<types::Side> _Side;
    s::data<types::Datetime> _TransactionTime;
    s::data<s::off::Ref<s::dyn::arr<Instrument>>> _Underlyings;
    s::data<s::off::Ref<s::dyn::arr<InstrumentLegBlock>>> _InstrumentLegs;
    
    // all accessors - get
    types::Type& MsgType() { return *_MsgType; }
    types::Type const& MsgType() const { return *_MsgType; }
    types::Id& ClOrdId() { return *_ClOrdId; }
    types::Id const& ClOrdId() const { return *_ClOrdId; }
    types::Side& Side() { return *_Side; }
    types::Side const& Side() const { return *_Side; }
    types::Datetime& TransactionTime() { return *_TransactionTime; }
    types::Datetime const& TransactionTime() const { return *_TransactionTime; }
    s::off::Ref<s::dyn::arr<Instrument>>& Underlyings() { return *_Underlyings; }
    s::off::Ref<s::dyn::arr<Instrument>> const& Underlyings() const { return *_Underlyings; }
    s::off::Ref<s::dyn::arr<InstrumentLegBlock>>& InstrumentLegs() { return *_InstrumentLegs; }
    s::off::Ref<s::dyn::arr<InstrumentLegBlock>> const& InstrumentLegs() const { return *_InstrumentLegs; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      NewOrderMultileg lNewOrderMultileg;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<types::Type>("MsgType", s::p<uint8_t>(s::addr(lNewOrderMultileg.MsgType())) - s::p<uint8_t>(s::addr(lNewOrderMultileg)), std::forward<T>(t));
      Describe::template apply<types::Id>("ClOrdId", s::p<uint8_t>(s::addr(lNewOrderMultileg.ClOrdId())) - s::p<uint8_t>(s::addr(lNewOrderMultileg)), std::forward<T>(t));
      Describe::template apply<types::Side>("Side", s::p<uint8_t>(s::addr(lNewOrderMultileg.Side())) - s::p<uint8_t>(s::addr(lNewOrderMultileg)), std::forward<T>(t));
      Describe::template apply<types::Datetime>("TransactionTime", s::p<uint8_t>(s::addr(lNewOrderMultileg.TransactionTime())) - s::p<uint8_t>(s::addr(lNewOrderMultileg)), std::forward<T>(t));
      Describe::template apply<s::off::Ref<s::dyn::arr<Instrument>>>("Underlyings", s::p<uint8_t>(s::addr(lNewOrderMultileg.Underlyings())) - s::p<uint8_t>(s::addr(lNewOrderMultileg)), std::forward<T>(t));
      Describe::template apply<s::off::Ref<s::dyn::arr<InstrumentLegBlock>>>("InstrumentLegs", s::p<uint8_t>(s::addr(lNewOrderMultileg.InstrumentLegs())) - s::p<uint8_t>(s::addr(lNewOrderMultileg)), std::forward<T>(t));
    }
    
    // setter
    template<class T> void set(bool& success, uint16_t n, T&& t) {
      using SetTag = s::tag::Set<typename std::decay<T>::type>;
      using SetGrp = s::grp::Set<typename std::decay<T>::type>;
      switch(n) {
        case 35: SetTag::template apply(success, MsgType(), std::forward<T>(t)); return;
        case 11: SetTag::template apply(success, ClOrdId(), std::forward<T>(t)); return;
        case 54: SetTag::template apply(success, Side(), std::forward<T>(t)); return;
        case 60: SetTag::template apply(success, TransactionTime(), std::forward<T>(t)); return;
        case 711: SetGrp::template apply(success, Underlyings(), std::forward<T>(t)); return;
        case 555: SetGrp::template apply(success, InstrumentLegs(), std::forward<T>(t)); return;
        default: success = false; return;
      }
    }
    
    // getter
    template<class T> void get(bool& success, T&& t) {
      using GetTag = s::tag::Get<typename std::decay<T>::type>;
      using GetGrp = s::grp::Get<typename std::decay<T>::type>;
      GetTag::template apply(success, MsgType(), 35, std::forward<T>(t));
      GetTag::template apply(success, ClOrdId(), 11, std::forward<T>(t));
      GetTag::template apply(success, Side(), 54, std::forward<T>(t));
      GetTag::template apply(success, TransactionTime(), 60, std::forward<T>(t));
      GetGrp::template apply(success, Underlyings(), 711, std::forward<T>(t));
      GetGrp::template apply(success, InstrumentLegs(), 555, std::forward<T>(t));
    }
  };
} // namespace onixs

namespace ronixs {
  namespace s = smunix;
  struct Instrument {
    s::data<s::fix::arr<char, 5>> _Symbol;
    s::data<s::fix::arr<char, 5>> _SymbolSfx;
    s::data<types::Date> _MaturityData;
    
    // all accessors - get
    s::fix::arr<char, 5>& Symbol() { return *_Symbol; }
    s::fix::arr<char, 5> const& Symbol() const { return *_Symbol; }
    s::fix::arr<char, 5>& SymbolSfx() { return *_SymbolSfx; }
    s::fix::arr<char, 5> const& SymbolSfx() const { return *_SymbolSfx; }
    types::Date& MaturityData() { return *_MaturityData; }
    types::Date const& MaturityData() const { return *_MaturityData; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      Instrument lInstrument;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<s::fix::arr<char, 5>>("Symbol", s::p<uint8_t>(s::addr(lInstrument.Symbol())) - s::p<uint8_t>(s::addr(lInstrument)), std::forward<T>(t));
      Describe::template apply<s::fix::arr<char, 5>>("SymbolSfx", s::p<uint8_t>(s::addr(lInstrument.SymbolSfx())) - s::p<uint8_t>(s::addr(lInstrument)), std::forward<T>(t));
      Describe::template apply<types::Date>("MaturityData", s::p<uint8_t>(s::addr(lInstrument.MaturityData())) - s::p<uint8_t>(s::addr(lInstrument)), std::forward<T>(t));
    }
    
    // setter
    template<class T> void set(bool& success, uint16_t n, T&& t) {
      using SetTag = s::tag::Set<typename std::decay<T>::type>;
      using SetGrp = s::grp::Set<typename std::decay<T>::type>;
      switch(n) {
        case 55: SetTag::template apply(success, Symbol(), std::forward<T>(t)); return;
        case 65: SetTag::template apply(success, SymbolSfx(), std::forward<T>(t)); return;
        case 541: SetTag::template apply(success, MaturityData(), std::forward<T>(t)); return;
        default: success = false; return;
      }
    }
    
    // getter
    template<class T> void get(bool& success, T&& t) {
      using GetTag = s::tag::Get<typename std::decay<T>::type>;
      using GetGrp = s::grp::Get<typename std::decay<T>::type>;
      GetTag::template apply(success, Symbol(), 55, std::forward<T>(t));
      GetTag::template apply(success, SymbolSfx(), 65, std::forward<T>(t));
      GetTag::template apply(success, MaturityData(), 541, std::forward<T>(t));
    }
  };
  struct InstrumentLeg {
    s::data<s::fix::arr<char, 5>> _LegSymbol;
    s::data<s::fix::arr<char, 5>> _LegSymbolSfx;
    s::data<types::Date> _LegMaturityData;
    
    // all accessors - get
    s::fix::arr<char, 5>& LegSymbol() { return *_LegSymbol; }
    s::fix::arr<char, 5> const& LegSymbol() const { return *_LegSymbol; }
    s::fix::arr<char, 5>& LegSymbolSfx() { return *_LegSymbolSfx; }
    s::fix::arr<char, 5> const& LegSymbolSfx() const { return *_LegSymbolSfx; }
    types::Date& LegMaturityData() { return *_LegMaturityData; }
    types::Date const& LegMaturityData() const { return *_LegMaturityData; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      InstrumentLeg lInstrumentLeg;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<s::fix::arr<char, 5>>("LegSymbol", s::p<uint8_t>(s::addr(lInstrumentLeg.LegSymbol())) - s::p<uint8_t>(s::addr(lInstrumentLeg)), std::forward<T>(t));
      Describe::template apply<s::fix::arr<char, 5>>("LegSymbolSfx", s::p<uint8_t>(s::addr(lInstrumentLeg.LegSymbolSfx())) - s::p<uint8_t>(s::addr(lInstrumentLeg)), std::forward<T>(t));
      Describe::template apply<types::Date>("LegMaturityData", s::p<uint8_t>(s::addr(lInstrumentLeg.LegMaturityData())) - s::p<uint8_t>(s::addr(lInstrumentLeg)), std::forward<T>(t));
    }
    
    // setter
    template<class T> void set(bool& success, uint16_t n, T&& t) {
      using SetTag = s::tag::Set<typename std::decay<T>::type>;
      using SetGrp = s::grp::Set<typename std::decay<T>::type>;
      switch(n) {
        case 600: SetTag::template apply(success, LegSymbol(), std::forward<T>(t)); return;
        case 601: SetTag::template apply(success, LegSymbolSfx(), std::forward<T>(t)); return;
        case 611: SetTag::template apply(success, LegMaturityData(), std::forward<T>(t)); return;
        default: success = false; return;
      }
    }
    
    // getter
    template<class T> void get(bool& success, T&& t) {
      using GetTag = s::tag::Get<typename std::decay<T>::type>;
      using GetGrp = s::grp::Get<typename std::decay<T>::type>;
      GetTag::template apply(success, LegSymbol(), 600, std::forward<T>(t));
      GetTag::template apply(success, LegSymbolSfx(), 601, std::forward<T>(t));
      GetTag::template apply(success, LegMaturityData(), 611, std::forward<T>(t));
    }
  };
  struct InstrumentLegBlock {
    s::data<InstrumentLeg> _Leg;
    s::data<types::Qty> _LegQty;
    s::data<types::SwapType> _LegSwapType;
    s::data<types::Side> _LegSide;
    
    // all accessors - get
    InstrumentLeg& Leg() { return *_Leg; }
    InstrumentLeg const& Leg() const { return *_Leg; }
    types::Qty& LegQty() { return *_LegQty; }
    types::Qty const& LegQty() const { return *_LegQty; }
    types::SwapType& LegSwapType() { return *_LegSwapType; }
    types::SwapType const& LegSwapType() const { return *_LegSwapType; }
    types::Side& LegSide() { return *_LegSide; }
    types::Side const& LegSide() const { return *_LegSide; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      InstrumentLegBlock lInstrumentLegBlock;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<InstrumentLeg>("Leg", s::p<uint8_t>(s::addr(lInstrumentLegBlock.Leg())) - s::p<uint8_t>(s::addr(lInstrumentLegBlock)), std::forward<T>(t));
      Describe::template apply<types::Qty>("LegQty", s::p<uint8_t>(s::addr(lInstrumentLegBlock.LegQty())) - s::p<uint8_t>(s::addr(lInstrumentLegBlock)), std::forward<T>(t));
      Describe::template apply<types::SwapType>("LegSwapType", s::p<uint8_t>(s::addr(lInstrumentLegBlock.LegSwapType())) - s::p<uint8_t>(s::addr(lInstrumentLegBlock)), std::forward<T>(t));
      Describe::template apply<types::Side>("LegSide", s::p<uint8_t>(s::addr(lInstrumentLegBlock.LegSide())) - s::p<uint8_t>(s::addr(lInstrumentLegBlock)), std::forward<T>(t));
    }
    
    // setter
    template<class T> void set(bool& success, uint16_t n, T&& t) {
      using SetTag = s::tag::Set<typename std::decay<T>::type>;
      using SetGrp = s::grp::Set<typename std::decay<T>::type>;
      switch(n) {
        case 687: SetTag::template apply(success, LegQty(), std::forward<T>(t)); return;
        case 690: SetTag::template apply(success, LegSwapType(), std::forward<T>(t)); return;
        case 624: SetTag::template apply(success, LegSide(), std::forward<T>(t)); return;
        default: success = false; return;
      }
    }
    
    // getter
    template<class T> void get(bool& success, T&& t) {
      using GetTag = s::tag::Get<typename std::decay<T>::type>;
      using GetGrp = s::grp::Get<typename std::decay<T>::type>;
      GetTag::template apply(success, LegQty(), 687, std::forward<T>(t));
      GetTag::template apply(success, LegSwapType(), 690, std::forward<T>(t));
      GetTag::template apply(success, LegSide(), 624, std::forward<T>(t));
    }
  };
  struct NewOrderMultileg {
    s::data<types::Type> _MsgType;
    s::data<types::Id> _ClOrdId;
    s::data<types::Side> _Side;
    s::data<types::Datetime> _TransactionTime;
    s::data<s::fix::arr<Instrument, 2>> _Underlyings;
    s::data<s::fix::arr<InstrumentLegBlock, 2>> _InstrumentLegs;
    
    // all accessors - get
    types::Type& MsgType() { return *_MsgType; }
    types::Type const& MsgType() const { return *_MsgType; }
    types::Id& ClOrdId() { return *_ClOrdId; }
    types::Id const& ClOrdId() const { return *_ClOrdId; }
    types::Side& Side() { return *_Side; }
    types::Side const& Side() const { return *_Side; }
    types::Datetime& TransactionTime() { return *_TransactionTime; }
    types::Datetime const& TransactionTime() const { return *_TransactionTime; }
    s::fix::arr<Instrument, 2>& Underlyings() { return *_Underlyings; }
    s::fix::arr<Instrument, 2> const& Underlyings() const { return *_Underlyings; }
    s::fix::arr<InstrumentLegBlock, 2>& InstrumentLegs() { return *_InstrumentLegs; }
    s::fix::arr<InstrumentLegBlock, 2> const& InstrumentLegs() const { return *_InstrumentLegs; }
    
    // all fields
    template<class T> static void describe(T&& t) {
      NewOrderMultileg lNewOrderMultileg;
      using Describe = s::fields::Describe<typename std::decay<T>::type>;
      Describe::template apply<types::Type>("MsgType", s::p<uint8_t>(s::addr(lNewOrderMultileg.MsgType())) - s::p<uint8_t>(s::addr(lNewOrderMultileg)), std::forward<T>(t));
      Describe::template apply<types::Id>("ClOrdId", s::p<uint8_t>(s::addr(lNewOrderMultileg.ClOrdId())) - s::p<uint8_t>(s::addr(lNewOrderMultileg)), std::forward<T>(t));
      Describe::template apply<types::Side>("Side", s::p<uint8_t>(s::addr(lNewOrderMultileg.Side())) - s::p<uint8_t>(s::addr(lNewOrderMultileg)), std::forward<T>(t));
      Describe::template apply<types::Datetime>("TransactionTime", s::p<uint8_t>(s::addr(lNewOrderMultileg.TransactionTime())) - s::p<uint8_t>(s::addr(lNewOrderMultileg)), std::forward<T>(t));
      Describe::template apply<s::fix::arr<Instrument, 2>>("Underlyings", s::p<uint8_t>(s::addr(lNewOrderMultileg.Underlyings())) - s::p<uint8_t>(s::addr(lNewOrderMultileg)), std::forward<T>(t));
      Describe::template apply<s::fix::arr<InstrumentLegBlock, 2>>("InstrumentLegs", s::p<uint8_t>(s::addr(lNewOrderMultileg.InstrumentLegs())) - s::p<uint8_t>(s::addr(lNewOrderMultileg)), std::forward<T>(t));
    }
    
    // setter
    template<class T> void set(bool& success, uint16_t n, T&& t) {
      using SetTag = s::tag::Set<typename std::decay<T>::type>;
      using SetGrp = s::grp::Set<typename std::decay<T>::type>;
      switch(n) {
        case 35: SetTag::template apply(success, MsgType(), std::forward<T>(t)); return;
        case 11: SetTag::template apply(success, ClOrdId(), std::forward<T>(t)); return;
        case 54: SetTag::template apply(success, Side(), std::forward<T>(t)); return;
        case 60: SetTag::template apply(success, TransactionTime(), std::forward<T>(t)); return;
        case 711: SetGrp::template apply(success, Underlyings(), std::forward<T>(t)); return;
        case 555: SetGrp::template apply(success, InstrumentLegs(), std::forward<T>(t)); return;
        default: success = false; return;
      }
    }
    
    // getter
    template<class T> void get(bool& success, T&& t) {
      using GetTag = s::tag::Get<typename std::decay<T>::type>;
      using GetGrp = s::grp::Get<typename std::decay<T>::type>;
      GetTag::template apply(success, MsgType(), 35, std::forward<T>(t));
      GetTag::template apply(success, ClOrdId(), 11, std::forward<T>(t));
      GetTag::template apply(success, Side(), 54, std::forward<T>(t));
      GetTag::template apply(success, TransactionTime(), 60, std::forward<T>(t));
      GetGrp::template apply(success, Underlyings(), 711, std::forward<T>(t));
      GetGrp::template apply(success, InstrumentLegs(), 555, std::forward<T>(t));
    }
  };
} // namespace ronixs
